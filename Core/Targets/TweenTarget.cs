﻿using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public abstract class TweenTarget<T> : ITweenTarget<T> where T : struct
    {
        public abstract GameObject Target { get; protected set; }

        public abstract T Value { get; }

        public abstract void UpdateValue(T from, T to, float value);

        public virtual void Dispose() { }

        public void ResetTarget(GameObject newTarget)
        {
            Target = newTarget;
        }

        protected static float Lerp(float from, float to, float value)
        {
            return from + (to - from) * value;
        }

        protected static Vector3 Lerp(Vector3 from, Vector3 to, float value)
        {
            return new Vector3(from.x + (to.x - from.x) * value,
                from.y + (to.y - from.y) * value,
                from.z + (to.z - from.z) * value);
        }

        protected static Vector2 Lerp(Vector2 from, Vector2 to, float value)
        {
            return new Vector2(from.x + (to.x - from.x) * value,
                from.y + (to.y - from.y) * value);
        }
    }
}
