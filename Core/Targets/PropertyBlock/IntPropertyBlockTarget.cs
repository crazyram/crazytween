﻿using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public class IntPropertyBlockTarget<T> : PropertyBlockTarget<int, T>
        where T : struct, IPropertyBlockConsumer
    {
        public IntPropertyBlockTarget(GameObject target, T consumer, int property, MaterialPropertyBlock block) : base(
            target,
            consumer, property, block)
        {
        }

        private static int Lerp(int from, int to, float value)
        {
            return (int) (from + (to - from) * value);
        }

        public override void UpdateValue(int from, int to, float value)
        {
            CachedValue = Lerp(from, to, value);
            Block.SetFloat(Property, CachedValue);
            Consumer.Consume(Block);
        }
    }
}
