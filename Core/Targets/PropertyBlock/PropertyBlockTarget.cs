﻿using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public abstract class PropertyBlockTarget<T, TV> : TweenTarget<T>
        where T : struct
        where TV : struct, IPropertyBlockConsumer
    {
        public sealed override GameObject Target { get; protected set; }

        protected TV Consumer;

        protected readonly int Property;

        protected readonly MaterialPropertyBlock Block;

        protected T CachedValue;

        public override T Value => CachedValue;

        protected PropertyBlockTarget(GameObject target, TV consumer, int property, MaterialPropertyBlock block)
        {
            Target = target.gameObject;
            Consumer = consumer;
            Property = property;
            Block = block;
        }

        public override void Dispose()
        {
            base.Dispose();
            Consumer.Dispose();
        }
    }
}
