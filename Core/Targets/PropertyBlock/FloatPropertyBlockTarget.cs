﻿using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public class FloatPropertyBlockTarget<T> : PropertyBlockTarget<float, T>
        where T : struct, IPropertyBlockConsumer
    {
        public FloatPropertyBlockTarget(GameObject target, T consumer, int property, MaterialPropertyBlock block) :
            base(target, consumer, property, block)
        {
        }

        public override void UpdateValue(float from, float to, float value)
        {
            CachedValue = Lerp(from, to, value);
            Block.SetFloat(Property, CachedValue);
            Consumer.Consume(Block);
        }
    }
}
