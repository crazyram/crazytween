﻿using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public class Vector3PropertyBlockTarget<T> : PropertyBlockTarget<Vector3, T>
        where T : struct, IPropertyBlockConsumer
    {
        public Vector3PropertyBlockTarget(GameObject target, T consumer, int property, MaterialPropertyBlock block) :
            base(target, consumer, property, block)
        {
        }

        public override void UpdateValue(Vector3 from, Vector3 to, float value)
        {
            CachedValue = Lerp(from, to, value);
            Block.SetVector(Property, CachedValue);
            Consumer.Consume(Block);
        }
    }
}
