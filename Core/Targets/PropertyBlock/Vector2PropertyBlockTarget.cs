﻿using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public class Vector2PropertyBlockTarget<T> : PropertyBlockTarget<Vector2, T>
        where T : struct, IPropertyBlockConsumer
    {
        public Vector2PropertyBlockTarget(GameObject target, T consumer, int property, MaterialPropertyBlock block) :
            base(target, consumer, property, block)
        {
        }

        public override void UpdateValue(Vector2 from, Vector2 to, float value)
        {
            CachedValue = Lerp(from, to, value);
            Block.SetVector(Property, CachedValue);
            Consumer.Consume(Block);
        }
    }
}
