﻿using CrazyRam.Core.Helpers;
using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public struct SingleBlockConsmer : IPropertyBlockConsumer
    {
        private readonly Renderer _target;

        public SingleBlockConsmer(Renderer target)
        {
            _target = target;
        }

        public void Consume(MaterialPropertyBlock block)
        {
            _target.Ok()?.SetPropertyBlock(block);
        }

        public void Dispose()
        {
        }
    }
}
