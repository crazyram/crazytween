﻿using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public interface IPropertyBlockConsumer
    {
        void Consume(MaterialPropertyBlock block);

        void Dispose();
    }
}
