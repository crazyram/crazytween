﻿using CrazyRam.Core.Helpers;
using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public struct ArrayBlockConsumer : IPropertyBlockConsumer
    {
        private readonly Renderer[] _renderers;

        public ArrayBlockConsumer(Renderer[] renderers)
        {
            _renderers = renderers;
        }

        public void Consume(MaterialPropertyBlock block)
        {
            for (int i = 0; i < _renderers.Length; i++)
                _renderers[i].Ok()?.SetPropertyBlock(block);
        }

        public void Dispose()
        {
        }
    }
}
