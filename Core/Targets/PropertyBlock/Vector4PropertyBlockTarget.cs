﻿using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public class Vector4PropertyBlockTarget<T> : PropertyBlockTarget<Vector4, T>
        where T : struct, IPropertyBlockConsumer
    {
        private static Vector4 Lerp(Vector4 from, Vector4 to, float value)
        {
            return new Vector4(
                from.x + (to.x - from.x) * value,
                from.y + (to.y - from.y) * value,
                from.z + (to.z - from.z) * value,
                from.w + (to.w - from.w) * value);
        }

        public Vector4PropertyBlockTarget(GameObject target, T consumer, int property, MaterialPropertyBlock block) :
            base(target, consumer, property, block)
        {
        }

        public override void UpdateValue(Vector4 from, Vector4 to, float value)
        {
            CachedValue = Lerp(from, to, value);
            Block.SetVector(Property, CachedValue);
            Consumer.Consume(Block);
        }
    }
}
