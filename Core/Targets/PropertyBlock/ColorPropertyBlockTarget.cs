﻿using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public class ColorPropertyBlockTarget<T> : PropertyBlockTarget<Color, T>
        where T : struct, IPropertyBlockConsumer
    {
        public ColorPropertyBlockTarget(GameObject target, T consumer, int property, MaterialPropertyBlock block) :
            base(
                target,
                consumer, property, block)
        {
        }

        public override void UpdateValue(Color from, Color to, float value)
        {
            CachedValue = Color.Lerp(from, to, value);
            Block.SetColor(Property, CachedValue);
            Consumer.Consume(Block);
        }
    }
}
