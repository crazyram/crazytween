﻿using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public class PositionZTarget : TransformTarget<float>
    {
        public override float Value { get { return Target.transform.position.x; } }

        public override void UpdateValue(float from, float to, float value)
        {
            if (Locally)
            {
                Vector3 pos = Target.transform.localPosition;
                pos.z = Lerp(from, to, value);
                Target.transform.localPosition = pos;
            }
            else
            {
                Vector3 pos = Target.transform.position;
                pos.z = Lerp(from, to, value);
                Target.transform.position = pos;
            }
        }

        public PositionZTarget(GameObject target) : base(target) { }
    }
}
