﻿using UnityEngine;
namespace CrazyRam.Core.Tween
{
    public class PositionTarget : TransformTarget<Vector3>
    {
        public override Vector3 Value { get { return Target.transform.position; } }

        public override void UpdateValue(Vector3 from, Vector3 to, float value)
        {
            if (Locally)
                Target.transform.localPosition = Lerp(from, to, value);
            else
                Target.transform.position = Lerp(from, to, value);
        }

        public PositionTarget(GameObject target) : base(target) { }
    }
}
