﻿using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public class EulerRotationTarget : TransformTarget<Vector3>
    {
        public override Vector3 Value { get { return Target.transform.rotation.eulerAngles; } }

        public override void UpdateValue(Vector3 from, Vector3 to, float value)
        {
            if (Locally)
                Target.transform.localRotation = Quaternion.Euler(Lerp(from, to, value));
            else
                Target.transform.rotation = Quaternion.Euler(Lerp(from, to, value));
        }

        public EulerRotationTarget(GameObject target) : base(target) {}
    }
}