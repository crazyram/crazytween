﻿using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public class RotationTarget : TransformTarget<Quaternion>
    {
        public override Quaternion Value { get { return Target.transform.rotation; } }

        public override void UpdateValue(Quaternion from, Quaternion to, float value)
        {
            if (Locally)
                Target.transform.localRotation = Quaternion.Lerp(from, to, value);
            else
                Target.transform.rotation = Quaternion.Lerp(from, to, value);
        }

        public RotationTarget(GameObject target) : base(target) { }
    }
}
