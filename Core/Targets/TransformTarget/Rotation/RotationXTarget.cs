﻿using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public class RotationXTarget : TransformTarget<float>
    {
        public override float Value { get { return Target.transform.rotation.eulerAngles.x; } }

        public override void UpdateValue(float from, float to, float value)
        {
            if (Locally)
            {
                Vector3 euler = Target.transform.localRotation.eulerAngles;
                euler.x = Lerp(from, to, value);
                Target.transform.localRotation = Quaternion.Euler(euler);
            }
            else
            {
                Vector3 euler = Target.transform.rotation.eulerAngles;
                euler.x = Lerp(from, to, value);
                Target.transform.rotation = Quaternion.Euler(euler);
            }
        }

        public RotationXTarget(GameObject target) : base(target) { }
    }
}