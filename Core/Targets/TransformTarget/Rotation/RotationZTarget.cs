﻿using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public class RotationZTarget : TransformTarget<float>
    {
        public override float Value { get { return Target.transform.rotation.eulerAngles.x; } }

        public override void UpdateValue(float from, float to, float value)
        {
            if (Locally)
            {
                Vector3 euler = Target.transform.localRotation.eulerAngles;
                euler.z = Lerp(from, to, value);
                Target.transform.localRotation = Quaternion.Euler(euler);
            }
            else
            {
                Vector3 euler = Target.transform.rotation.eulerAngles;
                euler.z = Lerp(from, to, value);
                Target.transform.rotation = Quaternion.Euler(euler);
            }
        }

        public RotationZTarget(GameObject target) : base(target) { }
    }
}