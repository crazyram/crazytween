﻿using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public class RotationYTarget : TransformTarget<float>
    {
        public override float Value { get { return Target.transform.rotation.eulerAngles.x; } }

        public override void UpdateValue(float from, float to, float value)
        {
            if (Locally)
            {
                Vector3 euler = Target.transform.localRotation.eulerAngles;
                euler.y = Lerp(from, to, value);
                Target.transform.localRotation = Quaternion.Euler(euler);
            }
            else
            {
                Vector3 euler = Target.transform.rotation.eulerAngles;
                euler.y = Lerp(from, to, value);
                Target.transform.rotation = Quaternion.Euler(euler);
            }
        }

        public RotationYTarget(GameObject target) : base(target) { }
    }
}