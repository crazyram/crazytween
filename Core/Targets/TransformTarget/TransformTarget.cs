﻿using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public abstract class TransformTarget<T> : TweenTarget<T> where T : struct
    {
        public sealed override GameObject Target { get; protected set; }

        protected bool Locally;

        protected TransformTarget(GameObject target)
        {
            Target = target;
        }

        public TweenTarget<T> SetLocal(bool localy)
        {
            Locally = localy;
            return this;
        }
    }
}
