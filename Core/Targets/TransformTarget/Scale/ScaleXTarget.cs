﻿using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public class ScaleXTarget : TransformTarget<float>
    {
        public override float Value { get { return Target.transform.localScale.x; } }

        public override void UpdateValue(float from, float to, float value)
        {
            Vector3 scale = Target.transform.localScale;
            scale.x = Lerp(from, to, value);
            Target.transform.localScale = scale;
        }

        public ScaleXTarget(GameObject target) : base(target) { }
    }
}
