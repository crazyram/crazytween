﻿using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public class ScaleTarget : TransformTarget<Vector3>
    {
        public override Vector3 Value => Target.transform.position;

        public override void UpdateValue(Vector3 from, Vector3 to, float value)
        {
            Target.transform.localScale = Lerp(from, to, value);
        }

        public ScaleTarget(GameObject target) : base(target) {}
    }
}
