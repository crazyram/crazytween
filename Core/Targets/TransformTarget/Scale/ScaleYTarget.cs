﻿using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public class ScaleYTarget : TransformTarget<float>
    {
        public override float Value { get { return Target.transform.localScale.y; } }

        public override void UpdateValue(float from, float to, float value)
        {
            Vector3 scale = Target.transform.localScale;
            scale.y = Lerp(from, to, value);
            Target.transform.localScale = scale;
        }

        public ScaleYTarget(GameObject target) : base(target) { }
    }
}
