﻿using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public class ScaleZTarget : TransformTarget<float>
    {
        public override float Value { get { return Target.transform.localScale.z; } }

        public override void UpdateValue(float from, float to, float value)
        {
            Vector3 scale = Target.transform.localScale;
            scale.z = Lerp(from, to, value);
            Target.transform.localScale = scale;
        }

        public ScaleZTarget(GameObject target) : base(target) { }
    }
}
