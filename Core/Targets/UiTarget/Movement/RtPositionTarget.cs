﻿using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public class RtPositionTarget : RectTransformTarget<Vector3>
    {
        public override Vector3 Value
        {
            get { return Transform.anchoredPosition3D; }
        }

        public override void UpdateValue(Vector3 from, Vector3 to, float value)
        {
            Transform.anchoredPosition3D = Lerp(from, to, value);
        }

        public RtPositionTarget(RectTransform transform) : base(transform) { }
    }
}
