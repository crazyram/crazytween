﻿using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public class RTPositionYTarget : RectTransformTarget<float>
    {
        public override float Value
        {
            get { return Transform.anchoredPosition3D.y; }
        }

        public override void UpdateValue(float from, float to, float value)
        {
            Vector3 pos = Transform.anchoredPosition3D;
            pos.y = Lerp(from, to, value);
            Transform.anchoredPosition3D = pos;
        }

        public RTPositionYTarget(RectTransform transform) : base(transform) { }
    }
}
