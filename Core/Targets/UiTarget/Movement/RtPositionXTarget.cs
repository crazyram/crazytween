﻿using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public class RTPositionXTarget : RectTransformTarget<float>
    {
        public override float Value
        {
            get { return Transform.anchoredPosition3D.x; }
        }

        public override void UpdateValue(float from, float to, float value)
        {
            Vector3 pos = Transform.anchoredPosition3D;
            pos.x = Lerp(from, to, value);
            Transform.anchoredPosition3D = pos;
        }

        public RTPositionXTarget(RectTransform transform) : base(transform) { }
    }
}
