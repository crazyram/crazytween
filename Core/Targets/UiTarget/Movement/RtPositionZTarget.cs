﻿using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public class RTPositionZTarget : RectTransformTarget<float>
    {
        public override float Value => Transform.anchoredPosition3D.z;

        public override void UpdateValue(float from, float to, float value)
        {
            var pos = Transform.anchoredPosition3D;
            pos.z = Lerp(from, to, value);
            Transform.anchoredPosition3D = pos;
        }

        public RTPositionZTarget(RectTransform transform) : base(transform) { }
    }
}

