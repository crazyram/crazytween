﻿using UnityEngine;

namespace CrazyRam.Core.Tween
{

    public class RtScaleTarget : RectTransformTarget<Vector2>
    {
        public override Vector2 Value
        {
            get { return Transform.sizeDelta; }
        }

        public override void UpdateValue(Vector2 from, Vector2 to, float value)
        {
            Transform.sizeDelta = Lerp(from, to, value);
        }

        public RtScaleTarget(RectTransform transform) : base(transform) { }
    }
}
