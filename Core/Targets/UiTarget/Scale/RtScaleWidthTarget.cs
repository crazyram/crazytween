﻿using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public class RtScaleWidthTarget : RectTransformTarget<float>
    {
        public override float Value
        {
            get { return Transform.sizeDelta.x; }
        }

        public override void UpdateValue(float from, float to, float value)
        {
            Transform.sizeDelta = new Vector2(Lerp(from, to, value), Transform.sizeDelta.y);
        }

        public RtScaleWidthTarget(RectTransform transform) : base(transform) { }
    }
}
