﻿using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public class RtScaleHeightTarget : RectTransformTarget<float>
    {
        public override float Value
        {
            get { return Transform.sizeDelta.y; }
        }

        public override void UpdateValue(float from, float to, float value)
        {
            Transform.sizeDelta = new Vector2(Transform.sizeDelta.x, Lerp(from, to, value));
        }

        public RtScaleHeightTarget(RectTransform transform) : base(transform) { }
    }
}
