﻿using UnityEngine.UI;

namespace CrazyRam.Core.Tween
{
    public class UiFillTarget : UiTarget<float>
    {
        private readonly Image _image;

        public override float Value { get { return _image.fillAmount; } }

        public override void UpdateValue(float from, float to, float value)
        {
            _image.fillAmount = Lerp(from, to, value);
        }

        public UiFillTarget(Image target) : base(target)
        {
            _image = target;
        }
    }
}
