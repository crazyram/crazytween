﻿using UnityEngine;
using UnityEngine.UI;

namespace CrazyRam.Core.Tween
{
    public class UiColorTarget : UiTarget<Color>
    {
        public override Color Value { get { return Renderer.color; } }

        public override void UpdateValue(Color from, Color to, float value)
        {
            Renderer.color = Color.Lerp(from, to, value);
        }

        public UiColorTarget(Graphic target) : base(target) { }
    }
}
