﻿using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public abstract class RectTransformTarget<T> : TweenTarget<T> where T : struct
    {
        public sealed override GameObject Target { get; protected set; }

        protected readonly RectTransform Transform;
        
        protected RectTransformTarget(RectTransform transform)
        {
            Transform = transform;
            Target = transform.gameObject;
        }
    }
}
