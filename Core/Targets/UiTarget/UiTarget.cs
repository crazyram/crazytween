﻿using UnityEngine;
using UnityEngine.UI;

namespace CrazyRam.Core.Tween
{
    public abstract class UiTarget<T> : TweenTarget<T> where T : struct
    {
        public sealed override GameObject Target { get; protected set; }

        protected readonly Graphic Renderer;

        protected UiTarget(Graphic target)
        {
            Renderer = target;
            Target = target.gameObject;
        }
    }
}
