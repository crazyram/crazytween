﻿using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public class IntMaterialTarget : MaterialTarget<int>
    {
        public IntMaterialTarget(GameObject target, int property, Material mat) : base(target, property, mat) { }

        private static int Lerp(int from, int to, float value)
        {
            return (int) (from + (to - from) * value);
        }

        public override void UpdateValue(int from, int to, float value)
        {
            CachedValue = Lerp(from, to, value);
            Material.SetInt(PropertyId, CachedValue);
        }
    }
}
