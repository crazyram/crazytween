﻿using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public abstract class MaterialTarget<T> : TweenTarget<T> where T : struct
    {
        public sealed override GameObject Target { get; protected set; }

        protected readonly Material Material;

        protected readonly int PropertyId;

        protected T CachedValue;

        public override T Value
        {
            get { return CachedValue; }
        }

        protected MaterialTarget(GameObject target, int property, Material mat)
        {
            Target = target;
            PropertyId = property;
            Material = mat;
        }
    }
}
