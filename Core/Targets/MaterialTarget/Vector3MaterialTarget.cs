﻿using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public class Vector3MaterialTarget : MaterialTarget<Vector3>
    {
        public Vector3MaterialTarget(GameObject target, int property, Material mat) : base(target, property, mat) { }

        public override void UpdateValue(Vector3 from, Vector3 to, float value)
        {
            CachedValue = Lerp(from, to, value);
            Material.SetVector(PropertyId, CachedValue);
        }
    }
}
