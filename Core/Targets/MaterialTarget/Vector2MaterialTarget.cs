﻿using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public class Vector2MaterialTarget : MaterialTarget<Vector2>
    {
        public Vector2MaterialTarget(GameObject target, int property, Material mat) : base(target, property, mat) { }

        public override void UpdateValue(Vector2 from, Vector2 to, float value)
        {
            CachedValue = Lerp(from, to, value);
            Material.SetVector(PropertyId, CachedValue);
        }
    }
}
