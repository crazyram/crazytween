﻿using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public class Vector4MaterialTarget : MaterialTarget<Vector4>
    {
        public Vector4MaterialTarget(GameObject target, int property, Material mat) : base(target, property, mat) { }

        private static Vector4 Lerp(Vector4 from, Vector4 to, float value)
        {
            return new Vector4(
                from.x + (to.x - from.x) * value,
                from.y + (to.y - from.y) * value,
                from.z + (to.z - from.z) * value,
                from.w + (to.w - from.w) * value);
        }

        public override void UpdateValue(Vector4 from, Vector4 to, float value)
        {
            CachedValue = Lerp(from, to, value);
            Material.SetVector(PropertyId, CachedValue);
        }
    }
}
