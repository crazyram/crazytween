﻿using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public class ColorMaterialTarget : MaterialTarget<Color>
    {
        public ColorMaterialTarget(GameObject target, int property, Material mat) : base(target, property, mat) { }

        public override void UpdateValue(Color from, Color to, float value)
        {
            CachedValue = Color.Lerp(from, to, value);
            Material.SetColor(PropertyId, CachedValue);
        }
    }
}
