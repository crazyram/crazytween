﻿using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public class FloatMaterialTarget : MaterialTarget<float>
    {
        public FloatMaterialTarget(GameObject target, int property, Material mat) : base(target, property, mat) { }

        public override void UpdateValue(float from, float to, float value)
        {
            CachedValue = Lerp(from, to, value);
            Material.SetFloat(PropertyId, CachedValue);
        }
    }
}
