﻿using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public class SpriteColorTarget : TweenTarget<Color>
    {
        public override GameObject Target
        {
            get { return _renderer != null ? _renderer.gameObject : null; }
            protected set { }
        }

        public override Color Value
        {
            get { return _renderer != null ? _renderer.color : Color.white; }
        }

        private SpriteRenderer _renderer;

        public SpriteColorTarget(SpriteRenderer target)
        {
            _renderer = target;
        }

        public override void UpdateValue(Color @from, Color to, float value)
        {
            _renderer.color = Color.Lerp(from, to, value);
        }
    }

}
