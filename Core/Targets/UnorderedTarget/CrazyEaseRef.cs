﻿using System;
using CrazyRam.Core.Utils.Ref;
using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public class CrazyEaseRef<T> : CrazyRef<T>, ITweenTarget<T> where T : struct
    {
        private readonly Func<float, T, T, T> _updater;

        public GameObject Target { get; private set; }

        /// <summary>
        /// Action params : value, from, to
        /// </summary>
        /// <param name="target"></param>
        /// <param name="geter"></param>
        /// <param name="seter"></param>
        /// <param name="updater"></param>
        public CrazyEaseRef(GameObject target, Func<T> geter, Action<T> seter, Func<float, T, T, T> updater) : base(geter, seter)
        {
            _updater = updater;
            Target = target;
        }

        public void UpdateValue(T from, T to, float value)
        {
            Value = _updater(value, from, to);
        }

        public void Dispose() {}

        public void ResetTarget(GameObject newTarget)
        {
            Target = newTarget;
        }
    }
}
