﻿using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public class EmptyTarget : TweenTarget<bool>
    {
        public sealed override GameObject Target { get; protected set; }

        public override bool Value => true;

        public override void UpdateValue(bool from, bool to, float value) { }

        public static readonly EmptyTarget Default = InitDefault();

        public EmptyTarget(GameObject target)
        {
            Target = target;
        }

        public EmptyTarget() { }

        public override void Dispose()
        {
            PoolFactory.Push(this);
        }

        private static EmptyTarget InitDefault()
        {
            var go = new GameObject();
            Object.DontDestroyOnLoad(go);
            return new EmptyTarget(go);
        }
    }
}
