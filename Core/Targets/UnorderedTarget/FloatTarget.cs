﻿using UnityEngine;

namespace CrazyRam.Core.Tween
{
    /// <summary>
    /// 0 - 1 Always!
    /// </summary>
    public class FloatTarget : TweenTarget<float>
    {
        public sealed override GameObject Target { get; protected set; }

        public override float Value
        {
            get { return _cachedResult; }
        }

        private float _cachedResult;

        public override void UpdateValue(float from, float to, float value)
        {
            _cachedResult = value;
        }

        public FloatTarget(GameObject target, float value)
        {
            Target = target;
            _cachedResult = value;
        }
    }
}
