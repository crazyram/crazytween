﻿using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public abstract class PhysicsTarget<T> : TweenTarget<T> where T : struct
    {
        public sealed override GameObject Target { get; protected set; }

        protected Rigidbody PhysicsBody;

        protected bool Locally;

        protected PhysicsTarget(Rigidbody target)
        {
            PhysicsBody = target;
            Target = target.gameObject;
        }

        public TweenTarget<T> SetLocal(bool localy)
        {
            Locally = localy;
            return this;
        }
    }
}
