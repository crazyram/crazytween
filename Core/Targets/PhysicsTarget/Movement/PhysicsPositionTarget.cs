﻿using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public class PhysicsPositionTarget : PhysicsTarget<Vector3>
    {
        public override Vector3 Value { get { return PhysicsBody.position; } }

        public override void UpdateValue(Vector3 from, Vector3 to, float value)
        {
            PhysicsBody.MovePosition(Lerp(from, to, value));
        }

        public PhysicsPositionTarget(Rigidbody target) : base(target) { }
    }
}
