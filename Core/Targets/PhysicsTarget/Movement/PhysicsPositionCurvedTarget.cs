﻿using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public class PhysicsPositionCurvedTarget : PhysicsTarget<Vector3>
    {
        private readonly AnimationCurve _xCurve;

        private readonly AnimationCurve _yCurve;

        private readonly AnimationCurve _zCurve;

        public PhysicsPositionCurvedTarget(Rigidbody target,
            AnimationCurve xCurve,
            AnimationCurve yCurve,
            AnimationCurve zCurve) : base(target)
        {
            _xCurve = xCurve;
            _yCurve = yCurve;
            _zCurve = zCurve;
        }

        public override Vector3 Value
        {
            get { return PhysicsBody.position; }
        }

        public override void UpdateValue(Vector3 from, Vector3 to, float value)
        {
            Vector3 delta = to - from;
            delta.x *= _xCurve.Evaluate(value);
            delta.y *= _yCurve.Evaluate(value);
            delta.z *= _zCurve.Evaluate(value);
            PhysicsBody.MovePosition(from + delta);
        }
    }
}
