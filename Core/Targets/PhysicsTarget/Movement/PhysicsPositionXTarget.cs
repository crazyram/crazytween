﻿using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public class PhysicsPositionXTarget : PhysicsTarget<float>
    {
        public override float Value { get { return Target.transform.position.x; } }

        public override void UpdateValue(float from, float to, float value)
        {
            Vector3 pos = PhysicsBody.position;
            pos.x = Lerp(from, to, value);
            PhysicsBody.MovePosition(pos);
        }

        public PhysicsPositionXTarget(Rigidbody target) : base(target) { }
    }
}
