﻿using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public class MultipleVector2MaterialTarget : MultipleMaterialsTarget<Vector2>
    {
        public MultipleVector2MaterialTarget(GameObject target, int property, params Material[] mat) : base(target,
            property, mat)
        {
        }

        public override void UpdateValue(Vector2 from, Vector2 to, float value)
        {
            CachedValue = Lerp(from, to, value);
            for (int i = 0; i < Materials.Length; i++)
                Materials[i].SetVector(PropertyId, CachedValue);
        }
    }
}
