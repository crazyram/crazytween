﻿using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public class MultipleVector4MaterialTarget : MultipleMaterialsTarget<Vector4>
    {
        private static Vector4 Lerp(Vector4 from, Vector4 to, float value)
        {
            return new Vector4(
                from.x + (to.x - from.x) * value,
                from.y + (to.y - from.y) * value,
                from.z + (to.z - from.z) * value,
                from.w + (to.w - from.w) * value);
        }

        public MultipleVector4MaterialTarget(GameObject target, int property, params Material[] mat) : base(target,
            property, mat)
        {
        }

        public override void UpdateValue(Vector4 from, Vector4 to, float value)
        {
            CachedValue = Lerp(from, to, value);
            for (int i = 0; i < Materials.Length; i++)
                Materials[i].SetVector(PropertyId, CachedValue);
        }
    }
}
