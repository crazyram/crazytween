﻿using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public class MultipleVector3MaterialTarget : MultipleMaterialsTarget<Vector3>
    {
        public MultipleVector3MaterialTarget(GameObject target, int property, params Material[] mat) : base(target,
            property, mat)
        {
        }

        public override void UpdateValue(Vector3 from, Vector3 to, float value)
        {
            CachedValue = Lerp(from, to, value);
            for (int i = 0; i < Materials.Length; i++)
                Materials[i].SetVector(PropertyId, CachedValue);
        }
    }
}
