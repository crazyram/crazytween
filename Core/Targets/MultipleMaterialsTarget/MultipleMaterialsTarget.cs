﻿using System;
using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public abstract class MultipleMaterialsTarget<T> : TweenTarget<T> where T : struct
    {
        public sealed override GameObject Target { get; protected set; }

        protected readonly Material[] Materials;

        protected readonly int PropertyId;

        protected T CachedValue;

        public override T Value
        {
            get { return CachedValue; }
        }

        protected MultipleMaterialsTarget(GameObject target, int property, params Material[] mat)
        {
            Target = target;
            PropertyId = property;
            Materials = mat;
            int count = 0;
            for (int i = 0; i < mat.Length; i++)
            {
                var m = mat[i];
                if (m != null && m.HasProperty(PropertyId) && Array.IndexOf(Materials, m) < 0)
                {
                    Materials[count] = mat[i];
                    count++;
                }
            }

            if (Materials.Length != count)
                Array.Resize(ref Materials, count);
        }

        public override void Dispose()
        {
            base.Dispose();
            for (int i = 0; i < Materials.Length; i++)
                Materials[i] = null;
        }
    }
}
