﻿using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public class MultipleIntMaterialTarget : MultipleMaterialsTarget<int>
    {
        public MultipleIntMaterialTarget(GameObject target, int property, params Material[] mat) : base(target,
            property, mat)
        {
        }

        private static int Lerp(int from, int to, float value)
        {
            return (int) (from + (to - from) * value);
        }

        public override void UpdateValue(int from, int to, float value)
        {
            CachedValue = Lerp(from, to, value);
            for (int i = 0; i < Materials.Length; i++)
                Materials[i].SetInt(PropertyId, CachedValue);
        }
    }
}
