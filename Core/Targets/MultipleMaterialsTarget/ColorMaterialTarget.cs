﻿using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public class MultipleColorMaterialTarget : MultipleMaterialsTarget<Color>
    {
        public MultipleColorMaterialTarget(GameObject target, int property, params Material[] mat) : base(target, property, mat) { }

        public override void UpdateValue(Color from, Color to, float value)
        {
            CachedValue = Color.Lerp(from, to, value);
            for (int i = 0; i < Materials.Length; i++)
                Materials[i].SetColor(PropertyId, CachedValue);
        }
    }
}
