﻿using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public class MultipleFloatMaterialTarget : MultipleMaterialsTarget<float>
    {
        public MultipleFloatMaterialTarget(GameObject target, int property, params Material[] mat) : base(target,
            property, mat)
        {
        }

        public override void UpdateValue(float from, float to, float value)
        {
            CachedValue = Lerp(from, to, value);
            for (int i = 0; i < Materials.Length; i++)
                Materials[i].SetFloat(PropertyId, CachedValue);
        }
    }
}
