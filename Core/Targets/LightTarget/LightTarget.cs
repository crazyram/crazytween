﻿using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public abstract class LightTarget<T> : TweenTarget<T> where T : struct
    {
        public sealed override GameObject Target { get; protected set; }

        protected Light Light;

        protected T CachedValue;

        public override T Value
        {
            get { return CachedValue; }
        }

        protected LightTarget(GameObject target, Light light)
        {
            Target = target;
            Light = light;
        }
    }
}
