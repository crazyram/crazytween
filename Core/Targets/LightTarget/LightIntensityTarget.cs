﻿using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public class LightIntensityTarget : LightTarget<float>
    {
        public LightIntensityTarget(GameObject target, Light light) : base(target, light) { }

        public override float Value
        {
            get { return Light.intensity; }
        }

        public override void UpdateValue(float from, float to, float value)
        {
            Light.intensity = Lerp(from, to, value);
        }
    }
}
