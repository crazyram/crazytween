﻿using CrazyRam.Core.Helpers;

namespace CrazyRam.Core.Tween
{
    public class RefTween<T> : Tween<T> where T : struct
    {
        private readonly CrazyEaseRef<T> _refValue;

        public RefTween(CrazyEaseRef<T> target, T from, T to, float time) : base(target, @from, to, time)
        {
            _refValue = target;
        }

        protected override void UpdateValue()
        {
            _refValue.UpdateValue(From, To, Curve?.Evaluate(NormalizedProgress) ?? Easing.Evaluate(Progress, Duration));
            OnUpdate.Raise(_refValue.Value);
            OnUpdateTrigger.Raise(_refValue.Value);
        }
    }
}
