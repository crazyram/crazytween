﻿using System;
using CrazyRam.Core.Helpers;
using CrazyRam.Core.Math;
using CrazyRam.Core.Time;
using UnityEngine;
using UnityEngine.Events;

namespace CrazyRam.Core.Tween
{
    //todo use type system and disable target allocations
    //todo use ref struct
    //todo reuse old tweens?
    public class Tween<T> : ITween<T> where T : struct
//    public class Tween<T, TV> : ITween<T> where T : struct where TV: struct, ITweenTarget<T>
    {
        private CycleType _cycleType = CycleType.None;

        private EaseType _easeType;

        protected IEaseType Easing;

        protected AnimationCurve Curve;

        public bool IsReverced;

        private bool _isPaused;


        protected T From;

        protected T To;

        private ITweenTarget<T> _target;


        protected float Duration;

        protected float Progress;

        protected float NormalizedProgress;

        private float _delay;

        private int _loopCount;

        private bool _active;

        private bool _everLoop;

        private bool _ignoreTimeScale;


        private Action _onStart;

        protected Action<T> OnUpdate;

        private Action _onComplete;

        private Action _onCycle;


        private UnityEvent _onStartTrigger;

        protected UnityEvent<T> OnUpdateTrigger;

        private UnityEvent _onCompleteTrigger;

        private UnityEvent _onCycleTrigger;

#if UNITY_EDITOR && CRAZY_DEBUG_TWEEN
        private string _debugData;
#endif

        private bool _grabbed;

        //abs Progress = Duration
        private bool CycleEnd => (Progress <= 0 && IsReverced) || (Progress >= Duration && !IsReverced);

        private bool StopOnCycle => (_loopCount < 0 || _cycleType == CycleType.None) && !_everLoop;

        public Tween()
        {
            _active = false;
        }

        public Tween(ITweenTarget<T> target, T from, T to, float time)
        {
            From = from;
            _target = target;
            To = to;
            Duration = time;
            _active = false;
            _grabbed = true;
        }


#if UNITY_EDITOR && CRAZY_DEBUG_TWEEN
        ~Tween()
        {
            Debug.LogWarning($"Tween end type:{typeof(T)} debug info: { _debugData }");
        }
#endif

        #region LifeCycle

        public virtual void Tick()
        {
            if (_isPaused)
                return;

            float delta = DeltaTime();
            if (_delay > 0)
            {
                _delay -= delta;
                if (_delay < 0)
                    OnDelayEnd();
                return;
            }

            Progress = Mathf.Clamp(IsReverced ? Progress - delta : Progress + delta, 0, Duration);
            NormalizedProgress = Progress / Duration;

            if (CycleEnd)
            {
                _loopCount--;
                if (_cycleType == CycleType.RepeatStart)
                    Progress = 0;
                else if (_cycleType != CycleType.None)
                    IsReverced = !IsReverced;

                if (StopOnCycle)
#if UNITY_EDITOR && CRAZY_DEBUG_TWEEN
                {
                    Debug.Log($"Ends tween: {typeof(T)} owner: {_target?.Target} debug info { _debugData }");
#endif
                    _active = false;
#if UNITY_EDITOR && CRAZY_DEBUG_TWEEN
                }
#endif

                if (_everLoop || _loopCount >= 0)
                {
                    _onCycle.Raise();
                    _onCycleTrigger.Raise();
                }
            }
            UpdateValue();
        }

        protected virtual void UpdateValue()
        {
            if (_target.Target == null)
            {
                Stop();
                return;
            }
            //cache this ternary condition?
            _target.UpdateValue(From, To, Curve?.Evaluate(NormalizedProgress) ?? Easing.Evaluate(Progress, Duration));
            OnUpdate.Raise(_target.Value);
            OnUpdateTrigger.Raise(_target.Value);
        }

        private float DeltaTime()
        {
            return _ignoreTimeScale
                ? CrazyTimeManager.UnscaledDeltaTime
                : CrazyTimeManager.DeltaTime;
        }

        public bool TargetEquals(object target)
        {
            return _target.Target == target as GameObject;
        }

        public virtual bool TimeEnd()
        {
            return !_active;
        }

        public virtual bool IsActive()
        {
            return _active;
        }

        #endregion

        #region Builder

        public ITween<T> SetTarget(ITweenTarget<T> target)
        {
            _target = target;
            return this;
        }

        public ITween<T> SetEaseType(IEaseType type)
        {
            Easing = type;
            _easeType = type.Ease;
            return this;
        }

        public ITween<T> SetDuration(float time)
        {
            Duration = time;
            return this;
        }

        public ITween<T> SetTimeNormalized(float time)
        {
            time = Mathf.Clamp01(time);
            Progress = Duration * time;
            NormalizedProgress = time;
            return this;
        }

        public ITween<T> SetFrom(T value)
        {
            From = value;
            return this;
        }

        public ITween<T> SetTo(T value)
        {
            To = value;
            return this;
        }

        public ITween<T> SetEaseType(EaseType easeType)
        {
            _easeType = easeType;
            Easing = _easeType.GetEase();
            return this;
        }

        public ITween<T> SetCycleType(CycleType cycleType)
        {
            _cycleType = cycleType;
            return this;
        }

        public ITween<T> SetCycleCount(int count)
        {
            _loopCount = count;
            return this;
        }

        public ITween<T> SetEverLoop(bool loop)
        {
            _everLoop = loop;
            return this;
        }

        public ITween<T> SetIgnoreTimeScale(bool ignore)
        {
            _ignoreTimeScale = ignore;
            return this;
        }

        public ITween<T> SetDelay(float delay)
        {
            _delay = delay;
            return this;
        }

        public ITween<T> SetAnimationCurve(AnimationCurve curve)
        {
            Curve = curve;
            return this;
        }

        public ITween<T> SetOnStart(Action onStart)
        {
            _onStart = onStart;
            return this;
        }

        #endregion

        #region Events

        public ITween<T> SetOnStart(UnityEvent onStart)
        {
            _onStartTrigger = onStart;
            return this;
        }

        public virtual ITween<T> SetOnUpdate(Action<T> onUpdate)
        {
            OnUpdate = onUpdate;
            return this;
        }

        public ITween<T> SetOnUpdate(UnityEvent<T> onComplete)
        {
            OnUpdateTrigger = onComplete;
            return this;
        }

        public virtual ITween<T> SetOnComplete(Action onComplete)
        {
            _onComplete = onComplete;
            return this;
        }

        public ITween<T> SetOnComplete(UnityEvent onComplete)
        {
            _onCompleteTrigger = onComplete;
            return this;
        }

        public ITween<T> SetOnCycle(Action onCycle)
        {
            _onCycle = onCycle;
            return this;
        }

        public ITween<T> SetOnCycle(UnityEvent onCycle)
        {
            _onCycleTrigger = onCycle;
            return this;
        }

        #endregion

        private void OnDelayEnd()
        {
            _onStart.Raise();
            _onStartTrigger.Raise();
        }

        private void OnComplete()
        {
            _onComplete.Raise();
            _onCompleteTrigger.Raise();
        }

        public ITween<T> Grab()
        {
            _grabbed = true;
            return this;
        }

        public ITween<T> Release()
        {
            _grabbed = false;
            return this;
        }

        /// <summary>
        /// Full clear
        /// </summary>
        private void Clear()
        {
            Dispose();

            _cycleType = CycleType.None;
            _easeType = EaseType.None;
            Easing = null;

            Curve = null;
            IsReverced = false;
            _isPaused = false;

            Progress = 0;
            NormalizedProgress = 0;

            _delay = 0;
            _loopCount = 0;
            _active = false;
            _everLoop = false;
            _ignoreTimeScale = false;

            _onStart = null;
            _onStartTrigger = null;

            _onCycle = null;
            _onCycleTrigger = null;

            OnUpdate = null;
            OnUpdateTrigger = null;

            _onComplete = null;
            _onCompleteTrigger = null;

#if UNITY_EDITOR && CRAZY_DEBUG_TWEEN
            _debugData = string.Empty;
#endif
        }

        public void Dispose()
        {
            _target?.Dispose();
            _target = null;
        }

        private void OnLifeTimeEnd()
        {
            if (!_grabbed)
            {
                Clear();
                PoolFactory.Push(this);
            }
        }

#if UNITY_EDITOR && CRAZY_DEBUG_TWEEN
        public void Log()
        {
            Debug.Log($"Tween: {typeof(T)} owner: {_target?.Target.Ok()?.name} debug info: {_debugData}");
        }

        public ITweenable SetDebugData(string data)
        {
            _debugData = data;
            return this;
        }
#endif

        #region Controls

        public void Start()
        {
            if (_active)
                return;
            if (Curve == null)
            {
                if (_easeType == EaseType.None)
                    _easeType = EaseType.Linear;
                Easing = _easeType.GetEase();
            }

            if (MathHelper.Approximately(Duration, 0))
            {
                if (_target.Target != null)
                    _target.UpdateValue(From, To, Curve?.Evaluate(1) ?? Easing.Evaluate(1, 1));
                OnComplete();
                return;
            }

            _active = true;
            if (MathHelper.Approximately(_delay, 0))
                OnDelayEnd();

            CrazyTween.Instance.AddTween(this);
        }

        public virtual void Stop(bool callOnComplete = false)
        {
            CrazyTween.Instance.RemoveTween(this);
            _active = false;
            if (callOnComplete)
                OnComplete();
            OnLifeTimeEnd();
        }

        public void Terminate()
        {
            if (IsReverced)
            {
                Progress = 0;
                NormalizedProgress = 0;
            }
            else
            {
                Progress = Duration;
                NormalizedProgress = 1;
            }
            UpdateValue();
            OnComplete();
            OnLifeTimeEnd();
        }

        public void Pause()
        {
            _isPaused = true;
        }

        public void Resume()
        {
            _isPaused = false;
        }

        #endregion
    }
}
