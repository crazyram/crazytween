﻿using System;
using System.Collections.Generic;
using CrazyRam.Core.Helpers;
using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public class ParallelTween : ITweenable
    {
        private readonly List<ITweenable> _tweens;

        private bool _active;

#if UNITY_EDITOR && CRAZY_DEBUG_TWEEN
        private string _debugData;
#endif

        private Action _onStart;

        private Action _onComplete;

        private readonly GameObject _target;

        public ParallelTween(GameObject target, ITweenable tween0, ITweenable tween1)
        {
            _target = target;
            //todo use List8
            _tweens = new List<ITweenable>(2) {tween0, tween1};
            _active = false;
        }

        public ParallelTween(GameObject target, params ITweenable[] tweens)
        {
            _target = target;
            //todo use List8
            _tweens = new List<ITweenable>(tweens);
            _active = false;
        }

        public void Push(params ITweenable[] tweens)
        {
            _tweens.AddRange(tweens);
        }

        public void Tick()
        {
            if (_tweens == null)
                return;
            bool allEnd = true;
            for (int i = 0; i < _tweens.Count; i++)
            {
                var tween = _tweens[i];
                if (tween != null && tween.IsActive())
                    allEnd = false;
            }
            if (allEnd)
                _active = false;
        }

        public void Pause()
        {
            if (_tweens.IsNullOrEmpty())
                return;
            for (int i = 0; i < _tweens.Count; i++)
                _tweens[i]?.Pause();
        }

        public void Resume()
        {
            if (_tweens.IsNullOrEmpty())
                return;
            for (int i = 0; i < _tweens.Count; i++)
                _tweens[i]?.Resume();
        }

        public void Start()
        {
            _active = true;
            _onStart.Raise();
            if (_tweens.IsNullOrEmpty())
                return;
            for (int i = 0; i < _tweens.Count; i++)
                _tweens[i]?.Start();
            CrazyTween.Instance.AddTween(this);
        }

        public void Stop(bool callOnComplete = false)
        {
            if (_tweens.IsNullOrEmpty())
                return;
            for (int i = 0; i < _tweens.Count; i++)
                _tweens[i]?.Stop(callOnComplete);
            if (callOnComplete)
                _onComplete.Raise();
        }

        public bool TargetEquals(object target)
        {
            return _target == target as GameObject;
        }

        public bool TimeEnd()
        {
            return !_active;
        }

        public bool IsActive()
        {
            return _active;
        }

        public ParallelTween SetOnStart(Action onStart)
        {
            _onStart = onStart;
            return this;
        }

        public ParallelTween SetOnComplete(Action onComplete)
        {
            _onComplete = onComplete;
            return this;
        }

        public void Dispose()
        {
            for (int i = 0; i < _tweens.Count; i++)
                _tweens[i]?.Dispose();
            _tweens.Clear();
        }

        public void Terminate()
        {
            _onComplete.Raise();
        }

#if UNITY_EDITOR && CRAZY_DEBUG_TWEEN
        public void Log()
        {
            Debug.Log($"Tween: Parallel tween owner: {_target.IsRealNull()?.name} debug info: { _debugData }");
        }

        public ITweenable SetDebugData(string data)
        {
            _debugData = data;
            return this;
        }
#endif
    }
}
