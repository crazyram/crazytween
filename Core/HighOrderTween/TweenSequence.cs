﻿using System;
using System.Collections.Generic;
using CrazyRam.Core.Helpers;
using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public class TweenSequence : ITweenable
    {
        private readonly List<ITweenable> _tweens;

        private int _id;

        private bool _active;

#if UNITY_EDITOR && CRAZY_DEBUG_TWEEN
        private string _debugData;
#endif

        private Action _onStart;

        private Action _onNextTween;

        private Action _onComplete;

        private readonly GameObject _target;

        public TweenSequence(GameObject target, params ITweenable[] tweens)
        {
            _target = target;
            //todo use List8
            _tweens = new List<ITweenable>(tweens);
            _id = 0;
            _active = false;
        }

        public void Push(params ITweenable[] tweens)
        {
            _tweens.AddRange(tweens);
        }

        public void Tick()
        {
            var tween = ActiveTween();
            if (tween == null || tween.TimeEnd())
            {
                _id++;
                bool inBounds = _tweens.InBounds(_id);
                if (inBounds)
                {
                    _onNextTween.Raise();
                    _tweens[_id].Start();
                }
                else
                    _active = false;
            }
        }

        private ITweenable ActiveTween()
        {
            return _tweens.InBounds(_id)
                ? _tweens[_id]
                : null;
        }

        public void Pause()
        {
            ActiveTween()?.Pause();
        }

        public void Resume()
        {
            ActiveTween()?.Resume();
        }

        public void Start()
        {
            _active = true;
            _onStart.Raise();
            ActiveTween()?.Start();
            if (_tweens != null && _tweens.Count > 0)
                CrazyTween.Instance.AddTween(this);
        }

        public void Stop(bool callOnComplete = false)
        {
            ActiveTween()?.Stop(callOnComplete);
            if (callOnComplete)
                _onComplete.Raise();
        }

        public bool TargetEquals(object target)
        {
            return _target == target as GameObject;
        }

        public bool TimeEnd()
        {
            return !_active;
        }

        public bool IsActive()
        {
            return _active;
        }

        public TweenSequence SetOnStart(Action onStart)
        {
            _onStart = onStart;
            return this;
        }

        public TweenSequence SetOnNextTween(Action onNextTween)
        {
            _onNextTween = onNextTween;
            return this;
        }

        public TweenSequence SetOnComplete(Action onComplete)
        {
            _onComplete = onComplete;
            return this;
        }

        public void Dispose()
        {
            for (int i = 0; i < _tweens.Count; i++)
                _tweens[i]?.Dispose();
            _tweens.Clear();
        }

        public void Terminate()
        {
            _onComplete.Raise();
        }

#if UNITY_EDITOR && CRAZY_DEBUG_TWEEN
        public void Log()
        {
            Debug.Log($"Tween: Sequence tween owner: {_target.IsRealNull()?.name} debug info: { _debugData }");
        }

        public ITweenable SetDebugData(string data)
        {
            _debugData = data;
            return this;
        }
#endif
    }
}
