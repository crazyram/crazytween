﻿public enum EaseType
{
    None = -1,
    Linear = 0,

    SineIn = 1,
    SineOut = 2,
    SineInOut = 3,

    CubicIn = 4,
    CubicOut = 5,
    CubicInOut = 6,

    QuadraticIn = 7,
    QuadraticOut = 8,
    QuadraticInOut = 9,

    BackIn = 10,
    BackOut = 11,
    BackInOut = 12,

    BounceIn = 13,
    BounceOut = 14,
    BounceInOut = 15,

    CircularIn = 16,
    CircularOut = 17,
    CircularInOut = 18,

    ElasticIn = 19,
    ElasticOut = 20,
    ElasticInOut = 21,
    ElasticPunch = 22,

    ExponentialIn = 23,
    ExponentialOut = 24,
    ExponentialInOut = 25,

    QuinticIn = 26,
    QuinticOut = 27,
    QuinticInOut = 28,

    QuarticIn = 29,
    QuarticOut = 30,
    QuarticInOut = 31
}
