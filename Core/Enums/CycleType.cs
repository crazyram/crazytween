﻿public enum CycleType
{
    None = 0,
    PingPong = 1,
    RepeatStart = 2
}
