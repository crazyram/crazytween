﻿namespace CrazyRam.Core.Tween
{
    public class BackInOutEase : IEaseType
    {
        public EaseType Ease => EaseType.BackInOut;

        public static readonly BackInOutEase Default = new BackInOutEase();

        public float Evaluate(float time, float duration)
        {
            float s = 1.70158f;

            if ((time /= duration / 2) < 1)
                return 0.5f * (time * time * (((s *= (1.525f)) + 1) * time - s));

            return 0.5f * ((time -= 2) * time * (((s *= (1.525f)) + 1) * time + s) + 2);
        }
    }
}
