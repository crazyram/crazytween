﻿namespace CrazyRam.Core.Tween
{
    public class BackOutEase : IEaseType
    {
        public EaseType Ease => EaseType.BackOut;

        public static readonly BackOutEase Default = new BackOutEase();

        public float Evaluate(float time, float duration)
        {
            return ((time = time / duration - 1) * time * ((1.70158f + 1) * time + 1.70158f) + 1);
        }
    }
}
