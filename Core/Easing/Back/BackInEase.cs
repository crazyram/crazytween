﻿namespace CrazyRam.Core.Tween
{
    public class BackInEase : IEaseType
    {
        public EaseType Ease => EaseType.BackIn;

        public static readonly BackInEase Default = new BackInEase();

        public float Evaluate(float time, float duration)
        {
            return (time /= duration) * time * ((1.70158f + 1) * time - 1.70158f);
        }
    }
}
