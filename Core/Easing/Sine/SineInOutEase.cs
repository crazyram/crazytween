﻿using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public class SineInOutEase : IEaseType
    {
        public EaseType Ease => EaseType.SineInOut;

        public static readonly SineInOutEase Default = new SineInOutEase();

        public float Evaluate(float time, float duration)
        {
            return -0.5f * (Mathf.Cos(Mathf.PI * time / duration) - 1);
        }
    }
}
