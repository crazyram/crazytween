﻿using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public class SineInEase : IEaseType
    {
        public EaseType Ease => EaseType.SineIn;

        public static readonly SineInEase Default = new SineInEase();

        public float Evaluate(float time, float duration)
        {
            return -1 * Mathf.Cos(time / duration * (Mathf.PI / 2)) + 1f;
        }
    }
}
