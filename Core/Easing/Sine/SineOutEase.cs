﻿using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public class SineOutEase : IEaseType
    {
        public EaseType Ease => EaseType.SineOut;

        public static readonly SineOutEase Default = new SineOutEase();

        public float Evaluate(float time, float duration)
        {
            return Mathf.Sin(time/duration*(Mathf.PI/2));
        }
    }
}
