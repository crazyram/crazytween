﻿using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public class CircularInEase : IEaseType
    {
        public EaseType Ease => EaseType.CircularIn;

        public static readonly CircularInEase Default = new CircularInEase();

        public float Evaluate(float time, float duration)
        {
            return -(Mathf.Sqrt(1 - (time /= duration) * time) - 1);
        }
    }
}
