﻿using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public class CircularOutEase : IEaseType
    {
        public EaseType Ease => EaseType.CircularOut;

        public static readonly CircularOutEase Default = new CircularOutEase();

        public float Evaluate(float time, float duration)
        {
            return Mathf.Sqrt(1 - (time = time / duration - 1) * time);
        }
    }
}
