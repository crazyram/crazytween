﻿using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public class CircularInOutEase : IEaseType
    {
        public EaseType Ease => EaseType.CircularInOut;

        public static readonly CircularInOutEase Default = new CircularInOutEase();

        public float Evaluate(float time, float duration)
        {
            if ((time /= duration / 2) < 1)
                return -0.5f * (Mathf.Sqrt(1 - time * time) - 1);

            return 0.5f * (Mathf.Sqrt(1 - (time -= 2) * time) + 1);
        }
    }
}
