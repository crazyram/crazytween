﻿namespace CrazyRam.Core.Tween
{
    public class QuarticInOutEase : IEaseType
    {
        public EaseType Ease => EaseType.QuarticInOut;

        public static readonly QuarticInOutEase Default = new QuarticInOutEase();

        public float Evaluate(float time, float duration)
        {
            time /= duration / 2;
            if (time < 1)
                return 0.5f * time * time * time * time;

            time -= 2;
            return -0.5f * (time * time * time * time - 2);
        }
    }
}
