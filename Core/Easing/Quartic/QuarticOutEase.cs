﻿namespace CrazyRam.Core.Tween
{
    public class QuarticOutEase : IEaseType
    {
        public EaseType Ease => EaseType.QuarticOut;

        public static readonly QuarticOutEase Default = new QuarticOutEase();

        public float Evaluate(float time, float duration)
        {
            return -1 * ((time = time / duration - 1) * time * time * time - 1);
        }
    }
}
