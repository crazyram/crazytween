﻿namespace CrazyRam.Core.Tween
{
    public class QuarticInEase : IEaseType
    {
        public EaseType Ease => EaseType.QuarticIn;

        public static readonly QuarticInEase Default = new QuarticInEase();

        public float Evaluate(float time, float duration)
        {
            return (time /= duration) * time * time * time;
        }
    }
}
