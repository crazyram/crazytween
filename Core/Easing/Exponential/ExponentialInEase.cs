﻿using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public class ExponentialInEase : IEaseType
    {
        public EaseType Ease => EaseType.ExponentialIn;

        public static readonly ExponentialInEase Default = new ExponentialInEase();

        public float Evaluate(float time, float duration)
        {
            return Mathf.Approximately(time, 0) ? 0 : Mathf.Pow(2, 10 * (time / duration - 1));
        }
    }
}
