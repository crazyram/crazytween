﻿using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public class ExponentialInOutEase : IEaseType
    {
        public EaseType Ease => EaseType.ExponentialInOut;

        public static readonly ExponentialInOutEase Default = new ExponentialInOutEase();

        public float Evaluate(float time, float duration)
        {
            if (Mathf.Approximately(time, 0))
                return 0;

            if (Mathf.Approximately(time, duration))
                return 1;

            if ((time /= duration / 2) < 1)
            {
                return 0.5f * Mathf.Pow(2, 10 * (time - 1));
            }
            return 0.5f * (-Mathf.Pow(2, -10 * --time) + 2);
        }
    }
}
