﻿using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public class ExponentialOutEase : IEaseType
    {
        public EaseType Ease => EaseType.ExponentialOut;

        public static readonly ExponentialOutEase Default = new ExponentialOutEase();

        public float Evaluate(float time, float duration)
        {
            return Mathf.Approximately(time, duration) ? 1 : -Mathf.Pow(2, -10 * time / duration) + 1;
        }
    }
}
