﻿namespace CrazyRam.Core.Tween
{
    public class QuadraticOutEase : IEaseType
    {
        public EaseType Ease => EaseType.QuadraticOut;

        public static readonly QuadraticOutEase Default = new QuadraticOutEase();

        public float Evaluate(float time, float duration)
        {
            return -1 * (time /= duration) * (time - 2);
        }
    }
}
