﻿namespace CrazyRam.Core.Tween
{
    public class QuadraticInOutEase : IEaseType
    {
        public EaseType Ease => EaseType.QuadraticInOut;

        public static readonly QuadraticInOutEase Default = new QuadraticInOutEase();

        public float Evaluate(float time, float duration)
        {
            if ((time /= duration / 2) < 1)
                return 0.5f * time * time;

            return -0.5f * ((--time) * (time - 2) - 1);
        }
    }
}
