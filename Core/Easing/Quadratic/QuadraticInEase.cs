﻿namespace CrazyRam.Core.Tween
{
    public class QuadraticInEase : IEaseType
    {
        public EaseType Ease => EaseType.QuadraticIn;

        public static readonly QuadraticInEase Default = new QuadraticInEase();

        public float Evaluate(float time, float duration)
        {
            return (time /= duration) * time;
        }
    }
}
