﻿namespace CrazyRam.Core.Tween
{
    public static class EaseTypeHelper
    {
        public static IEaseType GetEase(this EaseType ease)
        {
            switch (ease)
            {
                case EaseType.None:
                case EaseType.Linear:
                    return LinearEase.Default;

                case EaseType.SineIn:
                    return SineInEase.Default;
                case EaseType.SineOut:
                    return SineOutEase.Default;
                case EaseType.SineInOut:
                    return SineInOutEase.Default;

                case EaseType.CubicIn:
                    return CubicInEase.Default;
                case EaseType.CubicOut:
                    return CubicOutEase.Default;
                case EaseType.CubicInOut:
                    return CubicInOutEase.Default;

                case EaseType.QuadraticIn:
                    return QuadraticInEase.Default;
                case EaseType.QuadraticOut:
                    return QuadraticOutEase.Default;
                case EaseType.QuadraticInOut:
                    return QuadraticInOutEase.Default;

                case EaseType.BackIn:
                    return BackInEase.Default;
                case EaseType.BackOut:
                    return BackOutEase.Default;
                case EaseType.BackInOut:
                    return BackInOutEase.Default;

                case EaseType.BounceIn:
                    return BounceInEase.Default;
                case EaseType.BounceOut:
                    return BounceOutEase.Default;
                case EaseType.BounceInOut:
                    return BounceInOutEase.Default;

                case EaseType.CircularIn:
                    return CircularInEase.Default;
                case EaseType.CircularOut:
                    return CircularOutEase.Default;
                case EaseType.CircularInOut:
                    return CircularInOutEase.Default;

                case EaseType.ElasticIn:
                    return ElasticInEase.Default;
                case EaseType.ElasticOut:
                    return ElasticOutEase.Default;
                case EaseType.ElasticInOut:
                    return ElasticInOutEase.Default;
                case EaseType.ElasticPunch:
                    return ElasticPunchEase.Default;

                case EaseType.ExponentialIn:
                    return ExponentialInEase.Default;
                case EaseType.ExponentialOut:
                    return ExponentialOutEase.Default;
                case EaseType.ExponentialInOut:
                    return ExponentialInOutEase.Default;

                case EaseType.QuinticIn:
                    return QuinticInEase.Default;
                case EaseType.QuinticOut:
                    return QuinticOutEase.Default;
                case EaseType.QuinticInOut:
                    return QuinticInOutEase.Default;

                case EaseType.QuarticIn:
                    return QuarticInEase.Default;
                case EaseType.QuarticOut:
                    return QuarticOutEase.Default;
                case EaseType.QuarticInOut:
                    return QuarticInOutEase.Default;
                default:
                    return LinearEase.Default;
            }
        }
    }
}
