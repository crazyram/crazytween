﻿using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public class ElasticInEase : IEaseType
    {
        public EaseType Ease => EaseType.ElasticIn;

        public static readonly ElasticInEase Default = new ElasticInEase();

        public float Evaluate(float time, float duration)
        {
            if (Mathf.Approximately(time, 0))
                return 0;

            if (System.Math.Abs((time /= duration) - 1) < float.Epsilon)
                return 1;

            float p = duration * 0.3f;
            float s = p / 4;
            return -(1 * Mathf.Pow(2, 10 * (time -= 1)) * Mathf.Sin((time * duration - s) * (2 * Mathf.PI) / p));
        }
    }
}
