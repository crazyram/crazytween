﻿using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public class ElasticInOutEase : IEaseType
    {
        public EaseType Ease => EaseType.ElasticInOut;

        public static readonly ElasticInOutEase Default = new ElasticInOutEase();

        public float Evaluate(float time, float duration)
        {
            if (Mathf.Approximately(time, 0))
                return 0;

            if (Mathf.Approximately(time /= duration / 2, 2))
                return 1;

            float p = duration * (.3f * 1.5f);
            float s = p / 4;

            if (time < 1)
                return -.5f * (Mathf.Pow(2, 10 * (time -= 1)) * Mathf.Sin((time * duration - s) * (2 * Mathf.PI) / p));

            return Mathf.Pow(2f, -10f * (time -= 1f)) * Mathf.Sin((time * duration - s) * (2 * Mathf.PI) / p) * 0.5f +
                   1f;
        }
    }
}
