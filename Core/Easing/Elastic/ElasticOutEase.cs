﻿using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public class ElasticOutEase : IEaseType
    {
        public EaseType Ease => EaseType.ElasticOut;

        public static readonly ElasticOutEase Default = new ElasticOutEase();

        public float Evaluate(float time, float duration)
        {
            if (Mathf.Approximately(time, 0))
                return 0;

            if (System.Math.Abs((time /= duration) - 1) < float.Epsilon)
                return 1;

            float p = duration * 0.3f;
            float s = p / 4;
            return 1 * Mathf.Pow(2, -10 * time) * Mathf.Sin((time * duration - s) * (2 * Mathf.PI) / p) + 1;
        }
    }
}
