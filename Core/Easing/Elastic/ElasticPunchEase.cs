﻿using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public class ElasticPunchEase : IEaseType
    {
        public EaseType Ease => EaseType.ElasticPunch;

        public static readonly ElasticPunchEase Default = new ElasticPunchEase();

        public float Evaluate(float time, float duration)
        {
            if (Mathf.Approximately(time, 0))
                return 0;

            if (Mathf.Abs((time /= duration) - 1) < float.Epsilon)
                return 1;

            const float p = 0.3f;
            return Mathf.Pow(2, -10 * time) * Mathf.Sin(time * (2 * Mathf.PI) / p);
        }
    }
}
