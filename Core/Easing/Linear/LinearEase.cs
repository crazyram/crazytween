﻿namespace CrazyRam.Core.Tween
{
    public class LinearEase : IEaseType
    {
        public EaseType Ease => EaseType.Linear;

        public static readonly LinearEase Default = new LinearEase();

        public float Evaluate(float time, float duration)
        {
            return time / duration;
        }
    }
}
