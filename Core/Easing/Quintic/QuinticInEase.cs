﻿namespace CrazyRam.Core.Tween
{
    public class QuinticInEase : IEaseType
    {
        public EaseType Ease => EaseType.QuinticIn;

        public static readonly QuinticInEase Default = new QuinticInEase();

        public float Evaluate(float time, float duration)
        {
            return (time /= duration) * time * time * time * time;
        }
    }
}
