﻿namespace CrazyRam.Core.Tween
{
    public class QuinticOutEase : IEaseType
    {
        public EaseType Ease => EaseType.QuinticOut;

        public static readonly QuinticOutEase Default = new QuinticOutEase();

        public float Evaluate(float time, float duration)
        {
            return (time = time / duration - 1) * time * time * time * time + 1;
        }
    }
}
