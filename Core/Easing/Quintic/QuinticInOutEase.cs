﻿namespace CrazyRam.Core.Tween
{
    public class QuinticInOutEase : IEaseType
    {
        public EaseType Ease => EaseType.QuinticInOut;

        public static readonly QuinticInOutEase Default = new QuinticInOutEase();

        public float Evaluate(float time, float duration)
        {
            if ((time /= duration / 2) < 1)
                return 0.5f * time * time * time * time * time;

            return 0.5f * ((time -= 2) * time * time * time * time + 2);
        }
    }
}
