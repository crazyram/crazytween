﻿namespace CrazyRam.Core.Tween
{
    public class BounceOutEase : IEaseType
    {
        public EaseType Ease => EaseType.BounceOut;

        public static readonly BounceOutEase Default = new BounceOutEase();

        public static float Out(float time, float duration)
        {
            if ((time /= duration) < 1 / 2.75)
                return 7.5625f * time * time;
            if (time < 2 / 2.75)
                return 7.5625f * (time -= 1.5f / 2.75f) * time + .75f;
            if (time < 2.5 / 2.75)
                return 7.5625f * (time -= 2.25f / 2.75f) * time + .9375f;
            return 7.5625f * (time -= 2.625f / 2.75f) * time + .984375f;
        }

        public float Evaluate(float time, float duration)
        {
            return Out(time, duration);
        }
    }
}
