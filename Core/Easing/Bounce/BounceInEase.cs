﻿namespace CrazyRam.Core.Tween
{
    public class BounceInEase : IEaseType
    {
        public EaseType Ease => EaseType.BounceIn;

        public static readonly BounceInEase Default = new BounceInEase();

        public static float In(float time, float duration)
        {
            return 1 - BounceOutEase.Out(duration - time, duration);
        }

        public float Evaluate(float time, float duration)
        {
            return In(time, duration);
        }
    }
}
