﻿namespace CrazyRam.Core.Tween
{
    public class BounceInOutEase : IEaseType
    {
        public EaseType Ease => EaseType.BounceInOut;

        public static readonly BounceInOutEase Default = new BounceInOutEase();

        public float Evaluate(float time, float duration)
        {
            if (time < duration / 2)
                return BounceInEase.In(time * 2, duration) * 0.5f;
            return BounceOutEase.Out(time * 2 - duration, duration) * .5f + 1 * 0.5f;
        }
    }
}
