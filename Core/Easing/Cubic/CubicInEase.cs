﻿namespace CrazyRam.Core.Tween
{
    public class CubicInEase : IEaseType
    {
        public EaseType Ease => EaseType.CubicIn;

        public static readonly CubicInEase Default = new CubicInEase();

        public float Evaluate(float time, float duration)
        {
            return (time /= duration) * time * time;
        }
    }
}
