﻿namespace CrazyRam.Core.Tween
{
    public class CubicInOutEase : IEaseType
    {
        public EaseType Ease => EaseType.CubicInOut;

        public static readonly CubicInOutEase Default = new CubicInOutEase();

        public float Evaluate(float time, float duration)
        {
            if ((time /= duration / 2) < 1)
                return 0.5f * time * time * time;

            return 0.5f * ((time -= 2) * time * time + 2);
        }
    }
}
