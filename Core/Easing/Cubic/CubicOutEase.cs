﻿namespace CrazyRam.Core.Tween
{
    public class CubicOutEase : IEaseType
    {
        public EaseType Ease => EaseType.CubicOut;

        public static readonly CubicOutEase Default = new CubicOutEase();

        public float Evaluate(float time, float duration)
        {
            return (time = time / duration - 1) * time * time + 1;
        }
    }
}
