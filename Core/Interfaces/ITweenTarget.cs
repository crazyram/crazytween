﻿using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public interface ITweenTarget<T> where T : struct
    {
        GameObject Target { get; }

        T Value { get; }

        void UpdateValue(T from, T to, float value);

        void Dispose();

        void ResetTarget(GameObject newTarget);
    }
}
