﻿namespace CrazyRam.Core.Tween
{
    public interface ITweenable
    {
        void Tick();

        void Pause();

        void Resume();

        void Start();

        void Stop(bool callOnComplete = false);

        void Terminate();

        bool TargetEquals(object target);

        bool TimeEnd();

        bool IsActive();

        void Dispose();

#if UNITY_EDITOR && CRAZY_DEBUG_TWEEN
        void Log();

        ITweenable SetDebugData(string data);
#endif

    }
}
