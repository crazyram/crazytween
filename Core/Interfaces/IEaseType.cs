﻿namespace CrazyRam.Core.Tween
{
    public interface IEaseType
    {
        EaseType Ease { get; }

        float Evaluate(float time, float duration);
    }
}
