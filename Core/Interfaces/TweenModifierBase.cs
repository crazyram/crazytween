﻿namespace CrazyRam.Core.Tween
{
    [System.Serializable]
    public class TweenModifierBase
    {
        public virtual void Process<T>(ITween<T> tween) where T : struct { }
    }
}
