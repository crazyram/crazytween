﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace CrazyRam.Core.Tween
{
    public interface ITween<T> : ITweenable where T : struct
    {
        ITween<T> SetEaseType(IEaseType type);

        ITween<T> SetTarget(ITweenTarget<T> target);

        ITween<T> SetDuration(float time);

        ITween<T> SetTimeNormalized(float time);

        ITween<T> SetFrom(T value);

        ITween<T> SetTo(T value);


        ITween<T> SetEaseType(EaseType easeType);

        ITween<T> SetCycleType(CycleType cycleType);

        ITween<T> SetCycleCount(int count);

        ITween<T> SetEverLoop(bool loop);

        ITween<T> SetIgnoreTimeScale(bool ignore);

        ITween<T> SetDelay(float delay);

        ITween<T> SetAnimationCurve(AnimationCurve curve);


        ITween<T> SetOnStart(Action onStart);

        ITween<T> SetOnStart(UnityEvent onStart);


        ITween<T> SetOnUpdate(Action<T> onUpdate);

        ITween<T> SetOnUpdate(UnityEvent<T> onComplete);


        ITween<T> SetOnComplete(Action onComplete);

        ITween<T> SetOnComplete(UnityEvent onComplete);


        ITween<T> SetOnCycle(Action onCycle);

        ITween<T> SetOnCycle(UnityEvent onCycle);

        ITween<T> Grab();

        ITween<T> Release();
    }
}
