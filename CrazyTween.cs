﻿using System.Collections.Generic;
using CrazyRam.Core.Helpers;
using UnityEngine.SceneManagement;

namespace CrazyRam.Core.Tween
{
    public class CrazyTween : Singleton<CrazyTween>
    {
        private List<ITweenable> _activeTweens;

        public bool ClearOnNewLevel;

        private bool _closeGameNow;

        protected override void Awake()
        {
            base.Awake();
            _activeTweens = new List<ITweenable>();
            _closeGameNow = false;

            SceneManager.sceneLoaded += OnSceneLoaded;
        }

        public void OnDestroy()
        {
            SceneManager.sceneLoaded -= OnSceneLoaded;
            _closeGameNow = true;
            _activeTweens.Clear();
        }

        #region MonoBehaviour

        protected void OnApplicationQuit()
        {
            _closeGameNow = true;
        }

        private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
        {
            if (mode != LoadSceneMode.Additive && ClearOnNewLevel)
                _activeTweens.Clear();
        }

        protected void Update()
        {
            //process tweens here
            for (int i = 0; i < _activeTweens.Count; i++)
            {
                var tween = _activeTweens[i];
                tween.Tick();
                if (!tween.TimeEnd() || !_activeTweens.InBounds(i) || _activeTweens[i] != tween)
                    continue;
                _activeTweens.RemoveAt(i);
                tween.Terminate();
                i--;
            }
        }

        #endregion

        #region TweenControls

        public void AddTween(ITweenable tween)
        {
            if (!_closeGameNow)
            {
#if UNITY_EDITOR && CRAZY_DEBUG_TWEEN
                    tween.Log();
#endif
                _activeTweens.Add(tween);
            }
        }

        public void RemoveTween(ITweenable tween)
        {
            _activeTweens.SafeRemove(tween);
        }

        [System.Obsolete("StopAllTween is obsolete. Please use StopAllTweensAt instead")]
        public void StopAllTween(object target, bool callOnComplete = false)
        {
            StopAllTweensAt(target, callOnComplete);
        }

        public void StopAllTweensAt(object target, bool callOnComplete = false)
        {
            for (int i = 0; i < _activeTweens.Count; i++)
            {
                if (_activeTweens[i].TargetEquals(target))
                    _activeTweens[i].Stop(callOnComplete);
            }
        }

        public void StopAllTweens(bool callOnComplete)
        {
            for (int i = 0; i < _activeTweens.Count; i++)
                _activeTweens[i].Stop(callOnComplete);
            _activeTweens.Clear();
        }

        #endregion
    }
}
