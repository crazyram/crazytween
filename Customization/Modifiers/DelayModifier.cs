﻿using UnityEngine;

namespace CrazyRam.Core.Tween.Customization
{
    [System.Serializable]
    public class DelayModifier : TweenModifierBase
    {
        [SerializeField]
        private float _delay;

        public override void Process<T>(ITween<T> tween)
        {
            tween.SetDelay(_delay);
        }
    }
}
