﻿using UnityEngine;

namespace CrazyRam.Core.Tween.Customization
{
    [System.Serializable]
    public class EaseModifier : TweenModifierBase
    {
        [SerializeField]
        private bool _useCurve;

        [SerializeField]
        private AnimationCurve _curve;

        [SerializeField]
        private EaseType _ease = EaseType.Linear;

        public override void Process<T>(ITween<T> tween)
        {
            if (_useCurve)
                tween.SetAnimationCurve(_curve);
            else
                tween.SetEaseType(_ease);
        }
    }
}
