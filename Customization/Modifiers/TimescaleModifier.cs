﻿using UnityEngine;

namespace CrazyRam.Core.Tween.Customization
{
    [System.Serializable]
    public class TimescaleModifier : TweenModifierBase
    {
        [SerializeField]
        private bool _ignoreTimeScale;

        public override void Process<T>(ITween<T> tween)
        {
            tween.SetIgnoreTimeScale(_ignoreTimeScale);
        }
    }
}
