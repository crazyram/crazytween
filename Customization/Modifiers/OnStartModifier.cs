﻿using UnityEngine;
using UnityEngine.Events;

namespace CrazyRam.Core.Tween.Customization
{
    [System.Serializable]
    public class OnStartModifier : TweenModifierBase
    {
        [SerializeField]
        private UnityEvent _event;

        public override void Process<T>(ITween<T> tween)
        {
            tween.SetOnStart(_event);
        }
    }
}
