﻿using CrazyRam.Core.Helpers;
using UnityEngine;
using UnityEngine.Events;

namespace CrazyRam.Core.Tween.Customization
{
    [System.Serializable]
    public class OnUpdateModifier : TweenModifierBase
    {
        [SerializeField]
        private UnityEvent _event;

        public override void Process<T>(ITween<T> tween)
        {
            _event.Raise();
        }
    }
}
