﻿using UnityEngine;

namespace CrazyRam.Core.Tween.Customization
{
    [System.Serializable]
    public class CycleModifier : TweenModifierBase
    {
        [SerializeField]
        private int _count;

        [SerializeField]
        private CycleType _type;

        [SerializeField]
        private bool _ever;

        public override void Process<T>(ITween<T> tween)
        {
            tween.SetCycleCount(_count)
                .SetCycleType(_type)
                .SetEverLoop(_ever);
        }
    }
}
