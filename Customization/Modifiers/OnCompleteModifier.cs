﻿using UnityEngine;
using UnityEngine.Events;

namespace CrazyRam.Core.Tween.Customization
{
    [System.Serializable]
    public class OnCompleteModifier : TweenModifierBase
    {
        [SerializeField]
        private UnityEvent _event;

        public override void Process<T>(ITween<T> tween)
        {
            tween.SetOnComplete(_event);
        }
    }
}
