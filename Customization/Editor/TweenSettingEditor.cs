﻿using System.Collections.Generic;
using CrazyRam.Core.Tween.Customization;
using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(TweenSettings))]
public class TweenSettingEditor : PropertyDrawer
{

    private enum ModifierType
    {
        None = -1,
        Cycle = 0,
        Delay = 1,
        EaseType = 2,
        Timescale = 3,
        OnStart = 4,
        OnComplete = 5,
        OnCycle = 6
    }

    private ModifierType _selectedModifier = ModifierType.None;

    private const float MainWidth = 0.8f;

    private const float MiniButtonWidth = 30f;

    private const float Space = 2f;

    private const float ButtonOffset = 14;

    private Dictionary<string, string> _settings = new Dictionary<string, string>
    {
        {"_useCycle", "_cycle"},
        {"_useDelay", "_delay"},
        {"_useEase", "_ease"},
        {"_useTimescale", "_timescale"},
        {"_useOnComplete", "_onComplete"},
        {"_useOnStart", "_onStart"},
        {"_useOnCycle", "_onCycle"},
    };

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        float height = base.GetPropertyHeight(property, label);
        if (!property.isExpanded)
            return height;

        foreach (var setting in _settings)
        {
            if (property.FindPropertyRelative(setting.Key).boolValue)
                height += EditorGUI.GetPropertyHeight(property.FindPropertyRelative(setting.Value)) + Space;
        }
        return height + EditorGUIUtility.singleLineHeight;
    }

    public override void OnGUI(Rect pos, SerializedProperty prop, GUIContent label)
    {
        prop.isExpanded = EditorGUI.Foldout(new Rect(pos.x, pos.y, pos.width, EditorGUIUtility.singleLineHeight), prop.isExpanded, "Modifiers");
        if (!prop.isExpanded)
            return;
        pos.y += EditorGUIUtility.singleLineHeight;


        int ident = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 1;

        foreach (var setting in _settings)
            DrawProperty(setting.Key, setting.Value, prop, ref pos);

        _selectedModifier = (ModifierType)EditorGUI.EnumPopup(new Rect(pos.x, pos.y, pos.width * MainWidth, EditorGUIUtility.singleLineHeight)
            , "Modifier:", _selectedModifier, EditorStyles.popup);

        if (GUI.Button(new Rect(pos.x + pos.width - MiniButtonWidth - ButtonOffset, pos.y, MiniButtonWidth, EditorGUIUtility.singleLineHeight), "+"))
        {
            if (_selectedModifier == ModifierType.None)
                return;
            switch (_selectedModifier)
            {
                case ModifierType.Cycle:
                    AddProperty("_useCycle", prop);
                    break;
                case ModifierType.Delay:
                    AddProperty("_useDelay", prop);
                    break;
                case ModifierType.EaseType:
                    AddProperty("_useEase", prop);
                    break;
                case ModifierType.Timescale:
                    AddProperty("_useTimescale", prop);
                    break;
                case ModifierType.OnStart:
                    AddProperty("_useOnStart", prop);
                    break;
                case ModifierType.OnComplete:
                    AddProperty("_useOnComplete", prop);
                    break;
                case ModifierType.OnCycle:
                    AddProperty("_useOnCycle", prop);
                    break;
            }
            _selectedModifier = ModifierType.None;
        }

        EditorGUI.indentLevel = ident;
    }

    private void AddProperty(string name, SerializedProperty prop)
    {
        var property = prop.FindPropertyRelative(name);
        property.boolValue = true;
    }

    private void DrawProperty(string condition, string propertyName, SerializedProperty prop, ref Rect rect)
    {
        if (!prop.FindPropertyRelative(condition).boolValue)
            return;
        var property = prop.FindPropertyRelative(propertyName);

        EditorGUI.PropertyField(new Rect(rect.x, rect.y, rect.width * MainWidth, rect.height), property, true);

        if (GUI.Button(new Rect(rect.width - MiniButtonWidth, rect.y, MiniButtonWidth, EditorGUIUtility.singleLineHeight), "X"))
            prop.FindPropertyRelative(condition).boolValue = false;

        rect.y += EditorGUI.GetPropertyHeight(property) + Space;
    }

}
