﻿using UnityEngine;

namespace CrazyRam.Core.Tween.Customization
{
    public class CrazyMoveX : CrazyTweenBase<float>
    {
        [SerializeField]
        private bool _local;

        [SerializeField]
        private bool _fromCurrentPosition;

        [SerializeField]
        private float _from;

        [SerializeField]
        private float _to;

        protected override void BuildTween()
        {
            var tween = Taget.MoveToX(_to, Time, _local);
            if (!_fromCurrentPosition)
                tween.SetFrom(_from);
            Settings.Apply(tween);
            Tween = tween as Tween<float>;
        }
    }
}
