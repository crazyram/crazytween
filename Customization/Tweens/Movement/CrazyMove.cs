﻿using UnityEngine;

namespace CrazyRam.Core.Tween.Customization
{
    public class CrazyMove : CrazyTweenBase<Vector3>
    {
        [SerializeField]
        private bool _local;

        [SerializeField]
        private bool _fromCurrentPosition;

        [SerializeField]
        private Vector3 _from;

        [SerializeField]
        private Vector3 _to;

        protected override void BuildTween()
        {
            var tween = Taget.MoveTo(_to, Time, _local);
            if (!_fromCurrentPosition)
                tween.SetFrom(_from);
            Settings.Apply(tween);
            Tween = tween as Tween<Vector3>;
        }
    }
}
