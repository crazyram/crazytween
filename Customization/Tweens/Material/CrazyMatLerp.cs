﻿using UnityEngine;

namespace CrazyRam.Core.Tween.Customization
{
    public class CrazyMatLerp : CrazyTweenBase<Color>
    {
        [SerializeField]
        private Renderer _renderer;

        [SerializeField]
        private bool _shared;

        [SerializeField]
        private bool _useCurrentColor;

        [SerializeField]
        private Color _startColor;

        [SerializeField]
        private Color _to;

        protected override void BuildTween()
        {
            Color usedColor;
            if (_useCurrentColor && _renderer.sharedMaterial != null)
                usedColor = _renderer.sharedMaterial.color;
            else
                usedColor = _startColor;
            var tween = _renderer.ColorTo(usedColor, _to, Time, _shared);
            Settings.Apply(tween);
            Tween = tween as Tween<Color>;
        }
    }
}
