﻿using UnityEngine;

namespace CrazyRam.Core.Tween.Customization
{
    public class CrazyScale : CrazyTweenBase<Vector3>
    {
        [SerializeField]
        private bool _local;

        [SerializeField]
        private bool _fromCurrentScale;

        [SerializeField]
        private Vector3 _from;

        [SerializeField]
        private Vector3 _to;

        protected override void BuildTween()
        {
            var tween = Taget.ScaleTo(_to, Time, _local);
            if (!_fromCurrentScale)
                tween.SetFrom(_from);
            Settings.Apply(tween);
            Tween = tween as Tween<Vector3>;
        }
    }
}
