﻿using UnityEngine;

namespace CrazyRam.Core.Tween.Customization
{
    public abstract class CrazyTweenBase<T> : MonoBehaviour where T : struct
    {
        [SerializeField]
        protected GameObject Taget;

        [SerializeField]
        protected float Time;

        [SerializeField]
        protected bool PlayOnStart;

        [SerializeField]
        protected TweenSettings Settings;

        protected Tween<T> Tween;

        private void Start()
        {
            if (PlayOnStart)
                Play();
        }

        protected abstract void BuildTween();

        public virtual void Play()
        {
            if (Tween == null)
                BuildTween();
            Tween?.Start();
        }

        public void PlayReverce()
        {
            if (Tween == null)
                return;
            Tween.IsReverced = true;
            Tween.SetTimeNormalized(1);
            Tween.Start();
        }

        public void Pause()
        {
            Tween?.Pause();
        }

        public void Resume()
        {
            Tween?.Resume();
        }

        public void Stop(bool callOnComlete)
        {
            Tween?.Stop(callOnComlete);
        }
    }
}
