﻿using UnityEngine;

namespace CrazyRam.Core.Tween.Customization
{
    public class CrazyRotation : CrazyTweenBase<Vector3>
    {
        [SerializeField]
        private bool _local;

        [SerializeField]
        private bool _fromCurrentRotation;

        [SerializeField]
        private Vector3 _from;

        [SerializeField]
        private Vector3 _to;

        protected override void BuildTween()
        {
            var tween = Taget.RotateTo(_to, Time, _local);
            if (!_fromCurrentRotation)
                tween.SetFrom(_from);
            Settings.Apply(tween);
            Tween = tween as Tween<Vector3>;
        }
    }
}
