﻿using System;
using CrazyRam.Core.Serialization;
using UnityEngine;

namespace CrazyRam.Core.Tween.Customization
{
    [Serializable]
    public class TweenSettings
    {
        [Serializable]
        public class SerializedTweenModifier : SerializedComponent<TweenModifierBase>
        {
            [SerializeField, SerializedComponent(typeof(TweenModifierBase))]
            private Component _component;

            protected override Component Component => _component;
        }

        [SerializeField]
        private SerializedTweenModifier[] _modifers;

        public void Apply<T>(ITween<T> tween) where T : struct
        {
            for (int i = 0; i < _modifers.Length; i++)
                _modifers[i]?.GetItem()?.Process(tween);
        }
    }
}
