﻿using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public static class PropertyBlockTargetHelper
    {
        #region float

        public static ITween<float> FloatTo<T>(this MaterialPropertyBlock block, GameObject target, T consmer,
            int property, float from,
            float to, float time) where T : struct, IPropertyBlockConsumer
        {
            return CommonTweenHelper.TweenInstance(
                new FloatPropertyBlockTarget<T>(target, consmer, property,
                    block), from, to, time);
        }

        public static ITween<float> FloatTo<T>(this MaterialPropertyBlock block, GameObject target, T consumer,
            string property, float from,
            float to, float time) where T : struct, IPropertyBlockConsumer
        {
            int propertyId = Shader.PropertyToID(property);
            return CommonTweenHelper.TweenInstance(
                new FloatPropertyBlockTarget<T>(target, consumer, propertyId,
                    block), from, to, time);
        }

        #endregion

        #region int

        public static ITween<int> IntTo<T>(this MaterialPropertyBlock block, GameObject target, T consmer, int property,
            int from, int to, float time) where T : struct, IPropertyBlockConsumer
        {
            return CommonTweenHelper.TweenInstance(new IntPropertyBlockTarget<T>(target, consmer, property, block),
                from, to, time);
        }

        public static ITween<int> IntTo<T>(this MaterialPropertyBlock block, GameObject target, T consumer,
            string property, int from, int to, float time) where T : struct, IPropertyBlockConsumer
        {
            int propertyId = Shader.PropertyToID(property);
            return CommonTweenHelper.TweenInstance(new IntPropertyBlockTarget<T>(target, consumer, propertyId, block),
                from, to, time);
        }

        #endregion

        #region Vector2

        public static ITween<Vector2> Vector2To<T>(this MaterialPropertyBlock block, GameObject target, T consmer,
            int property, Vector2 from, Vector2 to, float time) where T : struct, IPropertyBlockConsumer
        {
            return CommonTweenHelper.TweenInstance(new Vector2PropertyBlockTarget<T>(target, consmer, property, block),
                from, to, time);
        }

        public static ITween<Vector2> Vector2To<T>(this MaterialPropertyBlock block, GameObject target, T consmer,
            string property, Vector2 from, Vector2 to, float time) where T : struct, IPropertyBlockConsumer
        {
            int propertyId = Shader.PropertyToID(property);
            return CommonTweenHelper.TweenInstance(
                new Vector2PropertyBlockTarget<T>(target, consmer, propertyId, block), from, to, time);
        }

        #endregion

        #region Vector3

        public static ITween<Vector3> Vector3To<T>(this MaterialPropertyBlock block, GameObject target, T consmer,
            int property, Vector3 from, Vector3 to, float time) where T : struct, IPropertyBlockConsumer
        {
            return CommonTweenHelper.TweenInstance(new Vector3PropertyBlockTarget<T>(target, consmer, property, block),
                from, to, time);
        }

        public static ITween<Vector3> Vector3To<T>(this MaterialPropertyBlock block, GameObject target, T consmer,
            string property, Vector3 from, Vector3 to, float time) where T : struct, IPropertyBlockConsumer
        {
            int propertyId = Shader.PropertyToID(property);
            return CommonTweenHelper.TweenInstance(
                new Vector3PropertyBlockTarget<T>(target, consmer, propertyId, block), from, to, time);
        }

        #endregion

        #region Vector4

        public static ITween<Vector4> Vector4To<T>(this MaterialPropertyBlock block, GameObject target, T consmer,
            int property,
            Vector4 from, Vector4 to, float time) where T : struct, IPropertyBlockConsumer
        {
            return CommonTweenHelper.TweenInstance(new Vector4PropertyBlockTarget<T>(target, consmer, property, block),
                from, to, time);
        }

        public static ITween<Vector4> Vector4To<T>(this MaterialPropertyBlock block, GameObject target, T consmer,
            string property, Vector4 from, Vector4 to, float time) where T : struct, IPropertyBlockConsumer
        {
            int propertyId = Shader.PropertyToID(property);
            return CommonTweenHelper.TweenInstance(
                new Vector4PropertyBlockTarget<T>(target, consmer, propertyId, block), from, to, time);
        }

        #endregion

        #region Color

        public static ITween<Color> ColorTo<T>(this MaterialPropertyBlock block, GameObject target, T consmer,
            int property, Color from,
            Color to, float time) where T : struct, IPropertyBlockConsumer
        {
            return CommonTweenHelper.TweenInstance(new ColorPropertyBlockTarget<T>(target, consmer, property, block),
                from, to, time);
        }

        public static ITween<Color> ColorTo<T>(this MaterialPropertyBlock block, GameObject target, T consmer,
            string property, Color from,
            Color to, float time) where T : struct, IPropertyBlockConsumer
        {
            int propertyId = Shader.PropertyToID(property);
            return CommonTweenHelper.TweenInstance(new ColorPropertyBlockTarget<T>(target, consmer, propertyId, block),
                from, to, time);
        }

        #endregion
    }
}
