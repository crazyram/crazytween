﻿namespace CrazyRam.Core.Tween
{
	public static class CommonTweenTargetHelper
	{
		public static EmptyTarget EmptyInstance(UnityEngine.GameObject go)
		{
			var target = PoolFactory.NextOrNew<EmptyTarget>();
			target.ResetTarget(go);
			return target;
		}
	}
}
