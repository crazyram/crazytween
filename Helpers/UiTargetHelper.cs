﻿using UnityEngine;
using UnityEngine.UI;

namespace CrazyRam.Core.Tween
{
    public static class UiTargetHelper
    {
        public static ITween<Color> ColorTo(this Graphic graphic, Color to, float time)
        {
            return CommonTweenHelper.TweenInstance(new UiColorTarget(graphic), graphic.color, to, time);
        }

        public static ITween<float> FillTo(this Image image, float to, float time)
        {
            return CommonTweenHelper.TweenInstance(new UiFillTarget(image), image.fillAmount, to, time);
        }
    }
}
