﻿using System;
using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public static class UnorderedTargetHelper
    {
        #region Wait

        public static ITween<bool> Wait(this GameObject go, float time)
        {
            var target = CommonTweenTargetHelper.EmptyInstance(go);
            return CommonTweenHelper.TweenInstance(target, true, true, time);
        }

        public static ITween<bool> Wait(this MonoBehaviour mb, float time)
        {
            var target = CommonTweenTargetHelper.EmptyInstance(mb.gameObject);
            return CommonTweenHelper.TweenInstance(target, true, true, time);
        }

        public static ITween<bool> Wait(this Transform tr, float time)
        {
            var target = CommonTweenTargetHelper.EmptyInstance(tr.gameObject);
            return CommonTweenHelper.TweenInstance(target, true, true, time);
        }

        public static ITween<bool> WaitNoTarget(this MonoBehaviour _, float time)
        {
            return CommonTweenHelper.TweenInstance(EmptyTarget.Default, true, true, time);
        }

        #endregion

        #region WaitAndDo

        public static void WaitAndDo(this GameObject go, float time, Action endAction
#if UNITY_EDITOR && CRAZY_DEBUG_TWEEN
            , string debugData = null
#endif
        )
        {
            var target = CommonTweenTargetHelper.EmptyInstance(go);
            CommonTweenHelper.TweenInstance(target, true, true, time)
                .SetOnComplete(endAction)
#if UNITY_EDITOR && CRAZY_DEBUG_TWEEN
                .SetDebugData(debugData)
#endif
                .Start();
        }

        public static void WaitAndDo(this MonoBehaviour mb, float time, Action endAction
#if UNITY_EDITOR && CRAZY_DEBUG_TWEEN
            , string debugData = null
#endif
        )
        {
            var target = CommonTweenTargetHelper.EmptyInstance(mb.gameObject);
            CommonTweenHelper.TweenInstance(target, true, true, time)
                .SetOnComplete(endAction)
#if UNITY_EDITOR && CRAZY_DEBUG_TWEEN
                .SetDebugData(debugData)
#endif
                .Start();
        }

        public static void WaitAndDo(this Transform tr, float time, Action endAction
#if UNITY_EDITOR && CRAZY_DEBUG_TWEEN
            , string debugData = null
#endif
        )
        {
            var target = CommonTweenTargetHelper.EmptyInstance(tr.gameObject);
            CommonTweenHelper.TweenInstance(target, true, true, time)
                .SetOnComplete(endAction)
#if UNITY_EDITOR && CRAZY_DEBUG_TWEEN
                .SetDebugData(debugData)
#endif
                .Start();
        }

        public static void WaitAndDoNoTarget(this Transform _, float time, Action endAction
#if UNITY_EDITOR && CRAZY_DEBUG_TWEEN
            , string debugData = null
#endif
        )
        {
            CommonTweenHelper.TweenInstance(EmptyTarget.Default, true, true, time)
                .SetOnComplete(endAction)
#if UNITY_EDITOR && CRAZY_DEBUG_TWEEN
                .SetDebugData(debugData)
#endif
                .Start();
        }

        #endregion

        #region WaitAndDoUnscaled

        public static void WaitAndDoUnscaled(this GameObject go, float time, Action endAction
#if UNITY_EDITOR && CRAZY_DEBUG_TWEEN
            , string debugData = null
#endif
        )
        {
            var target = CommonTweenTargetHelper.EmptyInstance(go);
            CommonTweenHelper.TweenInstance(target, true, true, time)
                .SetIgnoreTimeScale(true)
                .SetOnComplete(endAction)
#if UNITY_EDITOR && CRAZY_DEBUG_TWEEN
                .SetDebugData(debugData)
#endif
                .Start();
        }

        public static void WaitAndDoUnscaled(this MonoBehaviour mb, float time, Action endAction
#if UNITY_EDITOR && CRAZY_DEBUG_TWEEN
            , string debugData = null
#endif
        )
        {
            var target = CommonTweenTargetHelper.EmptyInstance(mb.gameObject);
            CommonTweenHelper.TweenInstance(target, true, true, time)
                .SetIgnoreTimeScale(true)
                .SetOnComplete(endAction)
#if UNITY_EDITOR && CRAZY_DEBUG_TWEEN
                .SetDebugData(debugData)
#endif
                .Start();
        }

        public static void WaitAndDoUnscaled(this Transform tr, float time, Action endAction
#if UNITY_EDITOR && CRAZY_DEBUG_TWEEN
            , string debugData = null
#endif
        )
        {
            var target = CommonTweenTargetHelper.EmptyInstance(tr.gameObject);
            CommonTweenHelper.TweenInstance(target, true, true, time)
                .SetIgnoreTimeScale(true)
                .SetOnComplete(endAction)
#if UNITY_EDITOR && CRAZY_DEBUG_TWEEN
                .SetDebugData(debugData)
#endif
                .Start();
        }

        public static void WaitAndDoNoTargetUscaled(this Transform _, float time, Action endAction
#if UNITY_EDITOR && CRAZY_DEBUG_TWEEN
            , string debugData = null
#endif
            )
        {
            CommonTweenHelper.TweenInstance(EmptyTarget.Default, true, true, time)
                .SetIgnoreTimeScale(true)
                .SetOnComplete(endAction)
#if UNITY_EDITOR && CRAZY_DEBUG_TWEEN
                .SetDebugData(debugData)
#endif
                .Start();
        }

        #endregion

        #region EndlessRegion

        public static ITween<bool> StartLoop(this MonoBehaviour _, Action action, float time, float delay = 0
#if UNITY_EDITOR && CRAZY_DEBUG_TWEEN
            , string debugData = null
#endif
        )
        {
            var tween = CommonTweenHelper.TweenInstance(EmptyTarget.Default, true, true, time)
                .SetIgnoreTimeScale(true)
                .SetCycleType(CycleType.RepeatStart)
                .SetEverLoop(true)
                .SetOnCycle(action)
                .SetDelay(delay);
#if UNITY_EDITOR && CRAZY_DEBUG_TWEEN
                tween.SetDebugData(debugData);
#endif
            tween.Start();
            return tween;
        }

        #endregion
    }
}
