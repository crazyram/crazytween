﻿using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public static class PhysicsTargetHelper
    {
        public static ITween<Vector3> MoveTo(this Rigidbody rbody, Vector3 to, float time, AnimationCurve x,
            AnimationCurve y, AnimationCurve z)
        {
            var startValue = rbody.position;
            return CommonTweenHelper.TweenInstance(new PhysicsPositionCurvedTarget(rbody, x, y, z), startValue, to, time);
        }

        public static ITween<Vector3> MoveTo(this Rigidbody rBody, Vector3 to, float time)
        {
            var startValue = rBody.position;
            return CommonTweenHelper.TweenInstance(new PhysicsPositionTarget(rBody), startValue, to, time);
        }

        public static ITween<float> MoveToX(this Rigidbody rBody, float to, float time)
        {
            float startValue = rBody.position.x;
            return CommonTweenHelper.TweenInstance(new PhysicsPositionXTarget(rBody), startValue, to, time);
        }
    }
}
