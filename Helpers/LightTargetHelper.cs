﻿using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public static class LightTargetHelper
    {
        public static ITween<float> IntensityTo(this Light light, float to, float time)
        {
            return CommonTweenHelper.TweenInstance(new LightIntensityTarget(light.gameObject, light), light.intensity,
                to, time);
        }
    }
}
