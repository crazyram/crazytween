﻿using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public static class RectTransformTargetHelper
    {
        #region Movement

        public static ITween<Vector3> CanvasMoveTo(this RectTransform tr, Vector3 to, float time)
        {
            return CommonTweenHelper.TweenInstance(new RtPositionTarget(tr), tr.anchoredPosition3D, to, time);
        }

        public static ITween<float> CanvasMoveToX(this RectTransform tr, float to, float time)
        {
            return CommonTweenHelper.TweenInstance(new RTPositionXTarget(tr), tr.anchoredPosition3D.x, to, time);
        }

        public static ITween<float> CanvasMoveToY(this RectTransform tr, float to, float time)
        {
            return CommonTweenHelper.TweenInstance(new RTPositionYTarget(tr), tr.anchoredPosition3D.y, to, time);
        }

        public static ITween<float> CanvasMoveToZ(this RectTransform tr, float to, float time)
        {
            return CommonTweenHelper.TweenInstance(new RTPositionZTarget(tr), tr.anchoredPosition3D.z, to, time);
        }

        #endregion

        #region Height

        public static ITween<Vector2> CanvasScale(this RectTransform tr, Vector2 to, float time)
        {
            return CommonTweenHelper.TweenInstance(new RtScaleTarget(tr), tr.sizeDelta, to, time);
        }

        public static ITween<float> CanvasScaleWidth(this RectTransform tr, float to, float time)
        {
            return CommonTweenHelper.TweenInstance(new RtScaleWidthTarget(tr), tr.sizeDelta.x, to, time);
        }

        public static ITween<float> CanvasScaleHeight(this RectTransform tr, float to, float time)
        {
            return CommonTweenHelper.TweenInstance(new RtScaleHeightTarget(tr), tr.sizeDelta.y, to, time);
        }

        #endregion
    }
}
