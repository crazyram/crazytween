﻿using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public static class MaterialTargetHelper
    {
        public static ITween<Color> SpriteColorTo(this SpriteRenderer re, Color from, Color to, float time)
        {
            return CommonTweenHelper.TweenInstance(new SpriteColorTarget(re), from, to, time);
        }

        #region float

        public static ITween<float> FloatTo(this GameObject go, int property, float from, float to, float time, Material material)
        {
            return CommonTweenHelper.TweenInstance(new FloatMaterialTarget(go, property, material), from, to, time);
        }

        public static ITween<float> FloatTo(this Renderer re, int property, float from, float to, float time, bool shared = true)
        {
            if (!re)
                return null;
            var materials = shared ? re.sharedMaterial : re.material;
            return CommonTweenHelper.TweenInstance(new FloatMaterialTarget(re.gameObject, property, materials), from, to, time);
        }

        public static ITween<float> FloatTo(this Material ma, GameObject ta, int property, float from, float to, float time)
        {
            return CommonTweenHelper.TweenInstance(new FloatMaterialTarget(ta, property, ma), from, to, time);
        }

        #endregion

        #region int

        public static ITween<int> IntTo(this GameObject go, int property, int from, int to, float time, Material material)
        {
            return CommonTweenHelper.TweenInstance(new IntMaterialTarget(go, property, material), from, to, time);
        }

        public static ITween<int> IntTo(this Renderer re, int property, int from, int to, float time, bool shared = true)
        {
            if (!re)
                return null;
            var materials = shared ? re.sharedMaterial : re.material;
            return CommonTweenHelper.TweenInstance(new IntMaterialTarget(re.gameObject, property, materials), from, to, time);
        }

        public static ITween<int> IntTo(this Material ma, GameObject ta, int property, int from, int to, float time)
        {
            return CommonTweenHelper.TweenInstance(new IntMaterialTarget(ta, property, ma), from, to, time);
        }

        #endregion

        #region Vector2

        public static ITween<Vector2> Vector2To(this GameObject go, int property, Vector2 from, Vector2 to, float time, Material material)
        {
            return CommonTweenHelper.TweenInstance(new Vector2MaterialTarget(go, property, material), from, to, time);
        }

        public static ITween<Vector2> Vector2To(this Renderer re, int property, Vector2 from, Vector2 to, float time, bool shared = true)
        {
            if (!re)
                return null;
            var materials = shared ? re.sharedMaterial : re.material;
            return CommonTweenHelper.TweenInstance(new Vector2MaterialTarget(re.gameObject, property, materials), from, to, time);
        }

        public static ITween<Vector2> Vector2To(this Material ma, GameObject ta, int property, Vector2 from, Vector2 to, float time)
        {
            return CommonTweenHelper.TweenInstance(new Vector2MaterialTarget(ta, property, ma), from, to, time);
        }

        #endregion

        #region Vector3

        public static ITween<Vector3> Vector3To(this GameObject go, int property, Vector3 from, Vector3 to, float time, Material material)
        {
            return CommonTweenHelper.TweenInstance(new Vector3MaterialTarget(go, property, material), from, to, time);
        }

        public static ITween<Vector3> Vector3To(this Renderer re, int property, Vector3 from, Vector3 to, float time, bool shared = true)
        {
            if (!re.gameObject)
                return null;
            var materials = shared ? re.sharedMaterial : re.material;
            return CommonTweenHelper.TweenInstance(new Vector3MaterialTarget(re.gameObject, property, materials), from, to, time);
        }

        public static ITween<Vector3> Vector3To(this Material ma, GameObject ta, int property, Vector3 from, Vector3 to, float time)
        {
            return CommonTweenHelper.TweenInstance(new Vector3MaterialTarget(ta, property, ma), from, to, time);
        }

        #endregion

        #region Vector4

        public static ITween<Vector4> Vector4To(this GameObject go, int property, Vector4 from, Vector4 to, float time, Material material)
        {
            return CommonTweenHelper.TweenInstance(new Vector4MaterialTarget(go, property, material), from, to, time);
        }

        public static ITween<Vector4> Vector4To(this Renderer re, int property, Vector4 from, Vector4 to, float time, bool shared = true)
        {
            if (!re.gameObject)
                return null;
            var materials = shared ? re.sharedMaterial : re.material;
            return CommonTweenHelper.TweenInstance(new Vector4MaterialTarget(re.gameObject, property, materials), from, to, time);
        }

        public static ITween<Vector4> Vector4To(this Material ma, GameObject ta, int property, Vector4 from, Vector4 to, float time)
        {
            return CommonTweenHelper.TweenInstance(new Vector4MaterialTarget(ta, property, ma), from, to, time);
        }

        #endregion

        #region Color

        public static ITween<Color> ColorTo(this GameObject go, int property, Color from, Color to, float time, Material material)
        {
            return CommonTweenHelper.TweenInstance(new ColorMaterialTarget(go, property, material), from, to, time);
        }

        public static ITween<Color> ColorTo(this Renderer re, Color from, Color to, float time, bool shared = true)
        {
            if (!re)
                return null;
            var materials = shared ? re.sharedMaterial : re.material;
            return CommonTweenHelper.TweenInstance(new ColorMaterialTarget(re.gameObject, Shader.PropertyToID("_Color"), materials), from, to, time);
        }

        public static ITween<Color> ColorTo(this Material ma, GameObject ta, int property, Color from, Color to, float time)
        {
            return CommonTweenHelper.TweenInstance(new ColorMaterialTarget(ta, property, ma), from, to, time);
        }

        #endregion
    }
}
