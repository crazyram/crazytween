﻿using UnityEngine;

namespace CrazyRam.Core.Tween
{
    public static class MultipleMaterialTargetHelper
    {
        #region float

        public static ITween<float> AllFloatTo(this GameObject go, int property, float from, float to, float time, params Material[] materials)
        {
            return CommonTweenHelper.TweenInstance(new MultipleFloatMaterialTarget(go, property, materials), from, to, time);
        }

        public static ITween<float> AllFloatTo(this Renderer re, int property, float from, float to, float time, bool shared = true)
        {
            if (!re)
                return null;
            var materials = shared ? re.sharedMaterials : re.materials;
            return CommonTweenHelper.TweenInstance(new MultipleFloatMaterialTarget(re.gameObject, property, materials), from, to, time);
        }

        #endregion

        #region int

        public static ITween<int> AllIntTo(this GameObject go, int property, int from, int to, float time, params Material[] materials)
        {
            return CommonTweenHelper.TweenInstance(new MultipleIntMaterialTarget(go, property, materials), from, to, time);
        }

        public static ITween<int> AllIntTo(this Renderer re, int property, int from, int to, float time, bool shared = true)
        {
            if (!re)
                return null;
            var materials = shared ? re.sharedMaterials : re.materials;
            return CommonTweenHelper.TweenInstance(new MultipleIntMaterialTarget(re.gameObject, property, materials), from, to, time);
        }

        #endregion

        #region Vector2

        public static ITween<Vector2> AllVector2To(this GameObject go, int property, Vector2 from, Vector2 to, float time, params Material[] materials)
        {
            return CommonTweenHelper.TweenInstance(new MultipleVector2MaterialTarget(go, property, materials), from, to, time);
        }

        public static ITween<Vector2> AllVector2To(this Renderer re, int property, Vector2 from, Vector2 to, float time, bool shared = true)
        {
            if (!re)
                return null;
            var materials = shared ? re.sharedMaterials : re.materials;
            return CommonTweenHelper.TweenInstance(new MultipleVector2MaterialTarget(re.gameObject, property, materials), from, to, time);
        }

        #endregion

        #region Vector3

        public static ITween<Vector3> AllVector3To(this GameObject go, int property, Vector3 from, Vector3 to, float time, params Material[] materials)
        {
            return CommonTweenHelper.TweenInstance(new MultipleVector3MaterialTarget(go, property, materials), from, to, time);
        }

        public static ITween<Vector3> AllVector3To(this Renderer re, int property, Vector3 from, Vector3 to, float time, bool shared = true)
        {
            if (!re.gameObject)
                return null;
            var materials = shared ? re.sharedMaterials : re.materials;
            return CommonTweenHelper.TweenInstance(new MultipleVector3MaterialTarget(re.gameObject, property, materials), from, to, time);
        }

        #endregion

        #region Vector4

        public static ITween<Vector4> AllVector4To(this GameObject go, int property, Vector4 from, Vector4 to, float time, params Material[] materials)
        {
            return CommonTweenHelper.TweenInstance(new MultipleVector4MaterialTarget(go, property, materials), from, to, time);
        }

        public static ITween<Vector4> AllVector4To(this Renderer re, int property, Vector4 from, Vector4 to, float time, bool shared = true)
        {
            if (!re.gameObject)
                return null;
            var materials = shared ? re.sharedMaterials : re.materials;
            return CommonTweenHelper.TweenInstance(new MultipleVector4MaterialTarget(re.gameObject, property, materials), from, to, time);
        }

        #endregion

        #region Color

        public static ITween<Color> AllColorTo(this GameObject go, int property, Color from, Color to, float time, params Material[] materials)
        {
            return CommonTweenHelper.TweenInstance(new MultipleColorMaterialTarget(go, property, materials), from, to, time);
        }

        public static ITween<Color> AllColorTo(this Renderer re, Color from, Color to, float time, bool shared = true)
        {
            if (!re)
                return null;
            var materials = shared ? re.sharedMaterials : re.materials;
            return CommonTweenHelper.TweenInstance(new MultipleColorMaterialTarget(re.gameObject, Shader.PropertyToID("_Color"), materials), from, to, time);
        }

        #endregion
    }
}
