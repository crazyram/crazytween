﻿using CrazyRam.Core.Tween;
using UnityEngine;

public static class TransformTargetHelper
{
    #region Movement

    //Transform extension
    public static ITween<Vector3> MoveTo(this Transform tr, Vector3 to, float time, bool local = false)
    {
        return tr.gameObject.MoveTo(to, time, local);
    }

    public static ITween<float> MoveToX(this Transform tr, float to, float time, bool local = false)
    {
        return tr.gameObject.MoveToX(to, time, local);
    }

    public static ITween<float> MoveToY(this Transform tr, float to, float time, bool local = false)
    {
        return tr.gameObject.MoveToY(to, time, local);
    }

    public static ITween<float> MoveToZ(this Transform tr, float to, float time, bool local = false)
    {
        return tr.gameObject.MoveToZ(to, time, local);
    }

    //GameObject extension
    public static ITween<Vector3> MoveTo(this GameObject go, Vector3 to, float time, bool local = false)
    {
        var startValue = local ? go.transform.localPosition : go.transform.position;
        return CommonTweenHelper.TweenInstance(new PositionTarget(go).SetLocal(local), startValue, to, time);
    }

    public static ITween<float> MoveToX(this GameObject go, float to, float time, bool local = false)
    {
        float startValue = local ? go.transform.localPosition.x : go.transform.position.x;
        return CommonTweenHelper.TweenInstance(new PositionXTarget(go).SetLocal(local), startValue, to, time);
    }

    public static ITween<float> MoveToY(this GameObject go, float to, float time, bool local = false)
    {
        float startValue = local ? go.transform.localPosition.y : go.transform.position.y;
        return CommonTweenHelper.TweenInstance(new PositionYTarget(go).SetLocal(local), startValue, to, time);
    }

    public static ITween<float> MoveToZ(this GameObject go, float to, float time, bool local = false)
    {
        float startValue = local ? go.transform.localPosition.z : go.transform.position.z;
        return CommonTweenHelper.TweenInstance(new PositionZTarget(go).SetLocal(local), startValue, to, time);
    }

    #endregion

    #region Rotation

    public static ITween<Quaternion> RotateTo(this Transform tr, Quaternion to, float time, bool local = false)
    {
        return tr.gameObject.RotateTo(to, time, local);
    }

    public static ITween<Vector3> RotateTo(this Transform tr, Vector3 to, float time, bool local = false, bool shortest = true)
    {
        return tr.gameObject.RotateTo(to, time, local, shortest);
    }

    public static ITween<float> RotateToX(this Transform tr, float to, float time, bool local = false, bool shortest = true)
    {
        return tr.gameObject.RotateToX(to, time, local, shortest);
    }

    public static ITween<float> RotateToY(this Transform tr, float to, float time, bool local = false, bool shortest = true)
    {
        return tr.gameObject.RotateToY(to, time, local, shortest);
    }

    public static ITween<float> RotateToZ(this Transform tr, float to, float time, bool local = false, bool shortest = true)
    {
        return tr.gameObject.RotateToZ(to, time, local, shortest);
    }

    public static ITween<Quaternion> RotateTo(this GameObject go, Quaternion to, float time, bool local = false)
    {
        var startValue = local ? go.transform.localRotation : go.transform.rotation;
        return CommonTweenHelper.TweenInstance(new RotationTarget(go).SetLocal(local), startValue, to, time);
    }

    public static ITween<Vector3> RotateTo(this GameObject go, Vector3 to, float time, bool local = false, bool shortest = true)
    {
        var startValue = local ? go.transform.localRotation.eulerAngles : go.transform.rotation.eulerAngles;
        if (shortest)
            to = new Vector3(ClosestAngle(startValue.x, to.x), ClosestAngle(startValue.z, to.z), ClosestAngle(startValue.z, to.z));
        return CommonTweenHelper.TweenInstance(new EulerRotationTarget(go).SetLocal(local), startValue, to, time);
    }

    public static ITween<float> RotateToX(this GameObject go, float to, float time, bool local = false, bool shortest = true)
    {
        float startValue = local ? go.transform.localRotation.eulerAngles.x : go.transform.rotation.eulerAngles.x;
        if (shortest)
            to = ClosestAngle(startValue, to);
        return CommonTweenHelper.TweenInstance(new RotationXTarget(go).SetLocal(local), startValue, to, time);
    }

    public static ITween<float> RotateToY(this GameObject go, float to, float time, bool local = false, bool shortest = true)
    {
        float startValue = local ? go.transform.localRotation.eulerAngles.y : go.transform.rotation.eulerAngles.y;
        if (shortest)
            to = ClosestAngle(startValue, to);
        return CommonTweenHelper.TweenInstance(new RotationYTarget(go).SetLocal(local), startValue, to, time);
    }

    public static ITween<float> RotateToZ(this GameObject go, float to, float time, bool local = false, bool shortest = true)
    {
        float startValue = local ? go.transform.localRotation.eulerAngles.z : go.transform.rotation.eulerAngles.z;
        if (shortest)
            to = ClosestAngle(startValue, to);
        return CommonTweenHelper.TweenInstance(new RotationZTarget(go).SetLocal(local), startValue, to, time);
    }

    public static float ClosestAngle(float from, float to)
    {
        to %= 360;
        float negate = to <= 0 ? to + 360 : to - 360;
        return Mathf.Abs(from - to) < Mathf.Abs(from - negate) ? to : negate;
    }

    #endregion

    #region Scale

    public static ITween<Vector3> ScaleTo(this Transform tr, Vector3 to, float time)
    {
        return tr.gameObject.ScaleTo(to, time);
    }

    public static ITween<float> ScaleToX(this Transform tr, float to, float time, bool local = false)
    {
        return ScaleToX(tr.gameObject, to, time, local);
    }

    public static ITween<float> ScaleToY(this Transform tr, float to, float time, bool local = false)
    {
        return ScaleToY(tr.gameObject, to, time, local);
    }

    public static ITween<float> ScaleToZ(this Transform tr, float to, float time, bool local = false)
    {
        return ScaleToZ(tr.gameObject, to, time, local);
    }

    public static ITween<Vector3> ScaleTo(this GameObject go, Vector3 to, float time, bool local = false)
    {
        return CommonTweenHelper.TweenInstance(new ScaleTarget(go).SetLocal(local), go.transform.localScale, to, time);
    }

    public static ITween<float> ScaleToX(this GameObject go, float to, float time, bool local = false)
    {
        return CommonTweenHelper.TweenInstance(new ScaleXTarget(go).SetLocal(local), go.transform.localScale.x, to, time);
    }

    public static ITween<float> ScaleToY(this GameObject go, float to, float time, bool local = false)
    {
        return CommonTweenHelper.TweenInstance(new ScaleYTarget(go).SetLocal(local), go.transform.localScale.y, to, time);
    }

    public static ITween<float> ScaleToZ(this GameObject go, float to, float time, bool local = false)
    {
        return CommonTweenHelper.TweenInstance(new ScaleZTarget(go).SetLocal(local), go.transform.localScale.z, to, time);
    }

    #endregion
}
