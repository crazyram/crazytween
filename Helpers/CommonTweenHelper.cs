﻿using System.Runtime.CompilerServices;

namespace CrazyRam.Core.Tween
{
    public static class CommonTweenHelper
    {
        public static void StopAllTweens(this UnityEngine.Transform tr, bool callOnComplete = false)
        {
            CrazyTween.Instance.StopAllTweensAt(tr.gameObject, callOnComplete);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void StopAllTweens(this UnityEngine.GameObject go, bool callOnComplete = false)
        {
            CrazyTween.Instance.StopAllTweensAt(go, callOnComplete);
        }

        public static void StopActive<T>(this ITween<T> tween, bool callOnComplete = false) where T : struct
        {
            if (tween != null && tween.IsActive())
                tween.Stop(callOnComplete);
        }

        public static ITween<T> TweenInstance<T>(ITweenTarget<T> target, T from, T to, float time) where T : struct
        {
            return PoolFactory.NextOrNew<Tween<T>>()
                .SetTarget(target)
                .SetFrom(from)
                .SetTo(to)
                .SetDuration(time);
        }
    }
}
