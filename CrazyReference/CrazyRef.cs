﻿using System;

namespace CrazyRam.Core.Utils.Ref
{
    public class CrazyRef<T>
    {
        private readonly Func<T> _get;

        private readonly Action<T> _set;

        public T Value
        {
            get { return _get(); }
            set { _set(value); }
        }

        public CrazyRef(Func<T> geter, Action<T> seter)
        {
            _get = geter;
            _set = seter;
        }
    }
}

////////////////
/// Example  ///
////////////////

//public class RefTestObject
//{
//    public float X { get; set; }
//}
//
//public class RefTest : MonoBehaviour
//{
//
//    [SerializeField, UsedImplicitly]
//    private float _x;
//
//    private IEnumerator Start()
//    {
//        RefTestObject r = new RefTestObject { X = 1 };
//
//        var xRef = new CrazyRef<float>(() => r.X, a => r.X = a) { Value = 15 };
//        yield return new WaitForSeconds(5);
//        Debug.Log(r.X);
//        xRef.Value = 1;
//        Debug.Log(r.X);
//    }
//
//}
