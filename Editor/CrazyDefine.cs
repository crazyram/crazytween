﻿using CrazyRam.Editor;
using UnityEditor;

namespace CrazyRam.Core.Tween
{
    [InitializeOnLoad]
    public sealed class CrazyDefine
    {
        private const string Define = "CRAZY_TWEEN_INCLUDED";

        static CrazyDefine()
        {
            CrazyEngineDefineSetter.SetDefine(Define);
        }
    }
}
